
<!-- One -->
        <section id="four" class="style1 bottom" style="margin-top:60px;padding: 20px;">
          <div class="content">
            <div class="container">
              <div class="row">
                <div class="col-4 col-12-medium">
                  <span class="image"><img src="http://castilloyasociadosmexico.com/logo-c.png" style="width: 300px;" alt="" /></span>
                </div>
                <div class="col-4 col-12-medium">
                  <h2>Misión</h2>
                  <p>“Brindar asistencia personalizada a cada uno de nuestros clientes por medio de
                    nuestros colaboradores expertos en el ámbito legal e inmobiliario de una manera
                    confiable y auténtica”
                  </p>
                </div>
                <div class="col-4 col-12-medium">
                  <h2>Visión</h2>
                  <p>
                  “Ser una empresa de vanguardia en el ámbito legal e inmobiliario, impulsando el
                  crecimiento de nuestros clientes con presencia a nivel estatal y compitiendo a nivel
                  regional.”</p>
                </div>
              </div>
            </div>
          </div>
          <a href="#two" class="goto-next scrolly">Next</a>
        </section>
    
        <section id="three" class="wrapper style1 special fade-up">
          <div class="container">
            <header class="major">
              <h2>Filosofia</h2>
              <p>Salvaguardar los intereses de nuestro cliente en materia legal o inmobiliario, de una
manera eficaz brindando una asistencia personalizada de manera integral y confiable.</p>
            </header>
            <div class="box alt">
              <div class="row gtr-uniform">
                <section class="col-4 col-6-medium col-12-xsmall">
                  <span class="icon alt major" style="padding: 9px;"><span  style="font-size: 40px;" class="fas fa-balance-scale"></span></span>
                  <h3>Justicia</h3>
                  <p>Buscar que cada persona reciba lo que merece.</p>
                </section>
                <section class="col-4 col-6-medium col-12-xsmall">
                  <span class="icon alt major" style="padding: 8px;"><span style="font-size: 40px;"  class="fas fa-equals"></span></span>
                  <h3>Equidad</h3>
                  <p>Trato personalizado a todos por igual, independiente de su clase social, raza, sexo o religión.</p>
                </section>
                <section class="col-4 col-6-medium col-12-xsmall">
                  <span class="icon alt major" style="padding: 8px;"><span style="font-size: 40px;"  class="fas fa-thumbs-up"></span></span>
                  <h3>Responsabilidad</h3>
                  <p>Compromiso con cada cliente de manera personalizada.</p>
                </section>
                <section class="col-4 col-6-medium col-12-xsmall">
                  <span class="icon alt major" style="padding: 8px;"><span style="font-size: 40px;"  class="fas fa-dumbbell"></span></span>
                  <h3>Perseverancia</h3>
                  <p>Capacidad de continuar esforzándose a pesar de los obstáculos.</p>
                </section>
                <section class="col-4 col-6-medium col-12-xsmall">
                  <span class="icon alt major" style="padding: 8px;"><span style="font-size: 40px;"  class="fas fa-hand-holding-heart"></span></span>
                  <h3>Empatia</h3>
                  <p>Involucrarse en las necesidades de otros, colaborando con la solución.</p>
                </section>
                <section class="col-4 col-6-medium col-12-xsmall">
                  <span class="icon alt major" style="padding: 8px;"><span style="font-size: 40px;"  class="fas fa-hands-helping"></span></span>
                  <h3>Integridad</h3>
                  <p>Coherencia en nuestras propias acciones.</p>
                </section>
              </div>
            </div>
            <footer class="major">
            </footer>
          </div>
        </section>

      <!-- Five -->
        <section id="five" class="wrapper style2 special fade">
          <div class="container">
            <header>
              <h2>Contacto</h2>
              <p></p>
            </header>
            <form method="post" action="#" class="cta">
              <div class="row gtr-uniform gtr-50">
                <div class="col-8 col-12-xsmall"><input type="email" name="email" id="email" placeholder="Su Correo Electronico" /></div>
                <div class="col-4 col-12-xsmall"><input type="submit" value="Suscribirse" class="fit primary" /></div>
              </div>
            </form>
          </div>
        </section>