<div id="shopping-cart">
<div class="txt-heading">Mi Carrito <a id="btnEmpty" href="/ajax-carrito.php?a=empty&r=1">Vaciar Carrito</a><a id="btnEmpty" href="/ajax-carrito.php?a=buscar" style="margin-left:8px;">Buscar Pedido</a></div>

{if $cart}
<table cellpadding="10" cellspacing="1">
<tbody>
<tr>
<th style="text-align:left;"><strong>Nombre</strong></th>
<th style="text-align:left;"><strong>Codigo</strong></th>
<th style="text-align:right;"><strong>Cantidad</strong></th>
<th style="text-align:right;"><strong>Precio</strong></th>
<th style="text-align:center;"><strong>Accion</strong></th>
</tr>

{foreach name=carrito from=$cart item=$item}

				<tr>
				<td style="text-align:left;border-bottom:#F0F0F0 1px solid;"><strong>{$item.name}</strong></td>
				<td style="text-align:left;border-bottom:#F0F0F0 1px solid;">{$item.code}</td>
				<td style="text-align:right;border-bottom:#F0F0F0 1px solid;">{$item.cantidad}</td>
				<td style="text-align:right;border-bottom:#F0F0F0 1px solid;">${$item.price}</td>
				<td style="text-align:center;border-bottom:#F0F0F0 1px solid;"><a href="/ajax-carrito.php?a=remove&code={$item.code}" class="button warning">Quitar</a></td>
				</tr>
{/foreach}

<tr>
<td colspan="5" align=right><strong>Total:</strong> ${$cart_total}</td>
</tr>
</tbody>
</table><br>
<a href="/index.php?action=carrito&p=2" class="button done" style="width:100%;">Pagar Ahora</a>
{else}
Carrito Vacio
{/if}
</div>
    