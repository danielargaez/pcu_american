<style type="text/css">
  #banner{
       background: url(/media/images/bg-inmob.jpg) center center !important;
  }

.image.fit {
    display: block;
    margin: 0 0 0 0;
    width: 100%;
}

.image.fit:hover {
  opacity: 0.5;
}
b{
  color:black;
}
.image {
    border-radius: 4px 4px 0px 0px !important;
    border: 0;
    display: inline-block;
    position: relative;
    overflow: hidden;
}
.image img {
    border-radius: 4px 4px 0px 0px !important;
    display: block;
}


</style>

<section id="banner">
          <div class="content"  style="text-align: center">
            <center>
              <h2 style="    text-shadow: 1px 1px 1px black;
    color: #fff;
    font-weight: 700;
    font-size: 62px;">Encuentre su proxima casa!</h2>
              <form method="get" action="/" >

              <div class="row" style="
    background: #ffffff7a;
    padding: 9px;
">
                <div class="col-8 col-12-xsmall" style="padding-left:8px;">
                  <input type="hidden" name="action" value="inmobiliario">
                  <input type="text"  style="background: #ffffff7a;    padding: 9px;    color:black;" name="s" placeholder="Ubicacion">
                </div>
                <div class="col-4 col-12-xsmall"><input type="submit" value="Buscar" class="fit primary" /></div>
              </div>
            </form>
          </center>
          </div>          
          <a href="#one" class="goto-next scrolly">Next</a>
        </section>


<section id="one" style="padding: 20px;">
          <div class="box alt">
                  <div class="row gtr-50 gtr-uniform">

                    {if $hayPosts}
                      {foreach key=id item=post from=$busqueda}
                        <div class="col-4 col-6-xsmall">
                          <a href="/propiedad/{$post.id}/{$post.titulou}"><span class="image fit" style="margin-bottom: 0px;"><img src="{$post.thumbail}" style="height: 300px;" alt=""></span></a>
                          <center style=" background: white;color: black;border-radius: 0px 0px 5px 5px;">
                            <p><b>Propiedad en {$post.ubicacion}</b>
                            <br><b><span style="color: blue;font-size:20px;">${$post.precio}</span></b>
                            <br><b><span style="color:blue">{$post.bath}</span> baños | <span style="color:blue">{$post.cuartos}</span> recamaras | <span style="color:blue">{$post.medidas} m²</span></b></p>
                          </center>
                        </div>
                      {/foreach}
                    {else}
                      <div class="col-4 col-6-xsmall"><h2>Sin resultados</h2><h6>No hay publicaciones que mostrar.</h6></div>
                    {/if}

                  </div>
                </div>
</section>



