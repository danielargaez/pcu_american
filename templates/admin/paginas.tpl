<style>
{literal}
.e{
    padding-top: 13px;
    color: white;
    margin-bottom: 8px;
    border-left: 8px solid white;
}
.v2{
  background: #038c6d;
}
a{  
    -webkit-transition: background 0.3s, height 3s; /* For Safari 3.1 to 6.0 */
    transition: background 0.3s, height 3s;
}
a:hover{
  color:white;
  background:#22214e;
}

.e h1, .e h2, .e h3, .e h4, .e h5, .e h6 {
    font-weight: bold;
    margin-bottom: 12px;
    color: #fff;
    font-family: \'Monda\', sans-serif;
}
{/literal}
</style><div class="grid-x small-up-1">
<a href="/index.php?action=admin&p=paginas&c=1" class="cell e v2"><div>
<center><h5><span class="fi-plus"></span> Crear nueva pagina</h5></center>
</div></a></div><hr><h2>Paginas Creadas</h2>
<ul>
{foreach name=pcreadas from=$paginasMain item=pag}
  {$pag}
{/foreach}
</ul>
