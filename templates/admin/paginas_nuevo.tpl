<h2>{if $e}Editar Pagina{else}Crear nueva pagina{/if}</h2>
    <!-- Init WysiBB BBCode editor -->  
    <script src="/js/tinymce/tinymce.min.js"></script>
      <script>
{literal}
        tinymce.init({
      selector: "textarea",
      height: 450,
      plugins: [
        "autoresize advlist autolink autosave link image lists charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
        "table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern"
      ],

      toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
      toolbar2: "forecolor backcolor | searchreplace | bullist numlist | outdent indent blockquote | link unlink anchor image media code | insertdatetime preview",
      toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | visualchars visualblocks nonbreaking pagebreak restoredraft | undo redo ",
      images_upload_url: '/ajax/subir-foto.php',
      images_upload_credentials: true,
      menubar: false,
      autoresize_max_height: 600
    });
      {/literal}
    </script>
    <script>
{literal}
    function cerrarPre(){
        $("#editor_previsualizar").fadeOut("fade");
        $("#editor_previsualizar_btn").fadeOut("fade");
        $("#editor_post").fadeIn("fade");
    }
{/literal}
    </script>

    <div id="editor_previsualizar"></div>
    <button id="editor_previsualizar_btn" style="display:none;" onclick="cerrarPre(); return false;" class="primary button">Cerrar Previsualizacion</button>
    <div id="editor">
    <form id="idForm" method="post" action="/index.php?action=admin&p=paginas&pub=1">
    <div id="editor_post">
    <label><h6>Titulo:</h6>
      <input type="text" name="titulo" value="{$titulo}" placeholder="Titulo de la pagina" maxlength="100" required>
    </label>
    <p class="help-text" id="passwordHelpText">El titulo tiene un maximo de 100 caracteres.</p>

    <label><h6>Contenido:</h6>
          <textarea id="edit" name="contenido" style="min-height:300px;" placeholder="Escribir algo...">{$contenido}</textarea>
    </label><br>
    <label><h6>Opciones</h6></label>
    <label><input type="checkbox" name="menu" value="1" {if $menu}checked{/if} class="check"> Mostrar en el menu principal</h6></label>
    <label><input type="checkbox" name="comentarios" value="1" {if $comentarios}checked{/if} class="check"> Habilitar caja de comentarios</h6></label>
    <br>

    {if $e}
       <input type="hidden" name="edit" value="{$idPag}">
    {/if}
    <input type="submit" class="primary button" value="Guardar">
    </div>
    </form></div>