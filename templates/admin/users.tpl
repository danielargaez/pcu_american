    <header>
      <h2>Administradores <span style="float:right;font-size: 15px;"><a href="/index.php?action=admin&p=users&c=1" class="cell e v2"><span class="fi-plus"></span> Agregar administrador</a></span></h2>
    </header>

    <section>
    <table>
      <thead>
        <tr>
          <th style="text-align: center;">#</th>
          <th style="text-align: center;">Nombre</th>
          <th style="text-align: center;">Correo</th>
          <th style="text-align: center;">Superadmin</th>
          <th style="text-align: center;">Opciones</th>
        </tr>
      </thead>
      <tbody>
    {foreach name=usersadmin from=$usersadmin item=u}
        {$u}
    {/foreach}
</tbody></table>
De momento solo uno puede ser superadmin.
</section>