
<div class="container" style="margin-top:100px;">
            
<section>
            <h2>{if $e} Editar Propiedad {else} Crear nueva propiedad{/if}</h2>
<!-- Init WysiBB BBCode editor -->  
<script src="/galeria_up.js"></script>
<script src="/js/tinymce/tinymce.min.js"></script>
  <script>
{literal}
  tinymce.init({
  selector: "textarea",
  height: 450,
  plugins: [
    "autoresize advlist autolink autosave link image lists charmap print preview hr anchor pagebreak",
    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
    "table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern"
  ],

  toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
  toolbar2: "forecolor backcolor | searchreplace | bullist numlist | outdent indent blockquote | link unlink anchor image media code | insertdatetime preview",
  toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | visualchars visualblocks nonbreaking pagebreak restoredraft | undo redo ",
  images_upload_url: '/ajax/subir-foto.php',
  images_upload_credentials: true,
  menubar: false,
  autoresize_max_height: 600
});
  {/literal}
</script>
<script>
{literal}
function cerrarPre(){
    $("#editor_previsualizar").fadeOut("fade");
    $("#editor_previsualizar_btn").fadeOut("fade");
    $("#editor_post").fadeIn("fade");
}
{/literal}
</script>

<div id="editor_previsualizar"></div>
<button id="editor_previsualizar_btn" style="display:none;" onclick="cerrarPre(); return false;" class="primary button">Cerrar Previsualizacion</button>
<div id="editor">
<form id="idForm" method="post" action="/index.php?action=admin&p=post&pub=1">
<div id="editor_post">
<label><h6>Titulo:</h6>
  <input type="text" name="titulo" value="{$tituloPost}" placeholder="Titulo del articulo" maxlength="100" required>
</label>
<p class="help-text" id="passwordHelpText">El titulo tiene un maximo de 100 caracteres.</p>

<label><h6>Contenido:</h6>
      <textarea id="edit" name="contenido" style="min-height:300px;" placeholder="Escribir algo...">{$contenido}</textarea>
</label>
<br>

<label><h6>Imagen Representativa o Miniatura:</h6>
  <input type="text" name="thumbnail" value="{$thumb}" placeholder="Ingresar enlace de imagen" maxlength="100" required>
</label>
<p class="help-text" id="passwordHelpText">Los thumbnails o miniaturas son versiones de imágenes, usadas para ayudar a su organización y reconocimiento.<br>
Se pueden subir imagenes en la opcion de la derecha llamada 'Subir Archivo'. </p>

<label><h6>Ubicacion:</h6>
  <input type="text" name="ubicacion" value="{$ubicacion}" placeholder="Merida, Yucatan" maxlength="100" required>
</label>

<label><h6>Precio:</h6>
  <input type="number" style="background:transparent; padding: 9px; border-width: 1px; border-radius: 8px;" name="precio" value="{$precio}" placeholder="100" maxlength="100" required>
</label>

<p class="help-text">Se necesitan minimo <b>3 etiquetas</b> separadas por comas. <br><b>Ejemplo:</b> musica, mexico, accion</p>
<label><h6>Categoria:</h6>
  <select name="categoria" required>';
    {foreach name=catsposts from=$cats item=cat}
     {$cat}
    {/foreach}
   </select>
</label><br>
{if $e}
<label><h6>GALERIA:</h6></label>
{$galeria}

<label><h6>Subir Fotos:</h6></label>
  <iframe src="/ajax/upload_foto.php?idP={$idPost}" style="width: 100%"></iframe>
{/if}
<br>

{if $e}
    <input type="hidden" name="edit" value="{$idPost}">
  {else}
    <input type="hidden" name="creador" value="{$nombreAActual}">
{/if}
<button type="submit" name="borrador"  value="1" class="success button" disabled>Guardar en Borradores</button>
<input type="submit" class="primary button" value="{if $e}Guardar{else}Publicar{/if}">
</div>
</form></div>
</section>
</div>