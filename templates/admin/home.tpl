{literal}
<style>
.e{
    padding: 9px;
    color: white;
    margin-bottom:8px;
    border-left:8px solid white;
}
.v{
  background: #47a947;
}
.am{
  background: #8c7c03;
}
.ce{
background: #03788c;
}
.v2{
  background: #038c6d;
}
.gr{
  background:#6b676a;
}
.ret{
   background: #790404;
}
.mor{
  background: #4f149a;
}
a{  
    -webkit-transition: background 0.3s, height 3s; /* For Safari 3.1 to 6.0 */
    transition: background 0.3s, height 3s;
}
a:hover{
  color:white;
  background:#22214e;
}

.e h1, .e h2, .e h3, .e h4, .e h5, .e h6 {
    font-weight: bold;
    margin-bottom: 12px;
    color: #fff;
    font-family: \'Monda\', sans-serif;
}

</style>
{/literal}

<div class="container" style="margin-top:100px;">
            
<section>
<h2>Bienvenido a la administracion!</h2><h5>Aqui se pueden configurar cada aspecto del sitio web</h5><hr>

<div class="grid-x small-up-3">

<a href="/index.php?action=admin&p=post" class="cell e v"><div>
<center><span class="fi-plus" style="font-size: 50px;"></span><h5>Crear nueva publicacion</h5></center>
<p>Agregar nueva publicacion en la pagina principal</p>
</div></a>
<a href="/index.php?action=admin&p=paginas" class="cell e v2"><div>
<center><span class="fi-page" style="font-size: 50px;"></span><h5>Paginas</h5></center>
<p>Agregar o editar paginas del sitio web</p>
</div></a>
<a href="/index.php?action=admin&p=transmitir" class="cell e ret"><div>
<center><span class="fi-eye" style="font-size: 50px;"></span><h5>Transmitir</h5></center>
<p>Transmitir Video</p>
</div></a>
<a href="/index.php?action=admin&p=publicidad" class="cell e am"><div>
<center><span class="fi-list" style="font-size: 50px;"></span><h5>Publicidad</h5></center>
<p>Editar bloques de publicidad</p>
</div></a>
<a href="/index.php?action=admin&p=categorias" class="cell e gr"><div>
<center><span class="fi-price-tag" style="font-size: 50px;"></span><h5>Categorias</h5></center>
<p>Agregar o editar alguna categoria</p>
</div></a>
<a href="/index.php?action=admin&p=users" class="cell e mor"><div>
<center><span class="fi-torso-business" style="font-size: 50px;"></span><h5>Administradores</h5></center>
<p>A�adir cuentas de administrador</p>
</div></a>
<a href="/index.php?action=admin&p=config" class="cell e ce"><div>
<center><span class="fi-widget" style="font-size: 50px;"></span><h5>Configuracion</h5></center>
<p>Configuraciones basicas</p>
</div></a>

</div></section>
</div>