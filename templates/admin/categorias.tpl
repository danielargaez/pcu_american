<h2>Categorias <a href="javascript:void(0);" class="btn outlined mleft_no reorder_link" id="save_reorder">Ordenar Categorias</a></h2>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script>
{literal}
$(document).ready(function(){
    $('.reorder_link').on('click',function(){
        $("ul.reorder-photos-list").sortable({ tolerance: 'pointer' });
        $('.reorder_link').html('Guardar Orden');
        $('.reorder_link').attr("id","save_reorder");
        $('#reorder-helper').slideDown('slow');
        $('.image_link').attr("href","javascript:void(0);");
        $('.image_link').css("cursor","move");
        $("#save_reorder").click(function( e ){
            if( !$("#save_reorder i").length ){
                $(this).html('').prepend('<img src="ajax-loader.gif"/>');
                $("ul.reorder-photos-list").sortable('destroy');
                $("#reorder-helper").html( "Reordenando... Porfavor espere..." ).removeClass('light_box').addClass('notice notice_error');
    
                var h = [];
                $("ul.reorder-photos-list li").each(function() {  h.push($(this).attr('id').substr(9));  });
                
                $.ajax({
                    type: "POST",
                    url: "/ajax-corden.php",
                    data: {ids: " " + h + ""},
                    success: function(){
                        window.location.reload();
                    }
                }); 
                return false;
            }   
            e.preventDefault();     
        });
    });
});
{/literal}
</script>
<style>
{literal}
.reorder_link {
    color: #3675B4;
    float: right;
    border: solid 2px #3675B4;
    border-radius: 3px;
    text-transform: uppercase;
    background: #fff;
    padding: 10px 20px;
    font-size:12px;
    margin: 15px 15px 15px 0px;
    font-weight: bold;
    text-decoration: none;
    transition: all 0.35s;
    -moz-transition: all 0.35s;
    -webkit-transition: all 0.35s;
    -o-transition: all 0.35s;
    white-space: nowrap;
}
.reorder_link:hover {
    color: #fff;
    border: solid 2px #3675B4;
    background: #3675B4;
    box-shadow: none;
}
#reorder-helper{margin-top:20px;padding: 10px;}
.light_box {
    background: #efefef;
    padding: 20px;
    margin: 10px 0;
    text-align: center;
    font-size: 1.2em;
     border: 1px solid;
}

.gallery{ width:100%; float:left; margin-top:8px;}
.gallery ul{ margin:0; padding:0; list-style-type:none;}
.gallery ul li {
    padding: 0px;
    margin: 10px 7px;
    width: auto;
    height: 45px;
}
.gallery img{ width:250px;}

/* NOTICE */
.notice, .notice a{ color: #fff !important; }
.notice { z-index: 8888; }
.notice a { font-weight: bold; }
.notice_error { background: #E46360; }
.notice_success { background: #657E3F; }
{/literal}
</style>
  <div>    
    <div id="reorder-helper" class="light_box" style="display:none;">1. Arrastre las categorias para ordenarlas.<br>2. Haga click en 'Guardar' cuando termine.</div>
    <div class="gallery">
        <ul class="reorder_ul reorder-photos-list">
            {foreach name=catadmins from=$catadmin item=cat}
                {$cat}
            {/foreach}
        
        </ul>
    </div>
</div>                      
                            <form method="post" action="/index.php?action=admin&p=categorias&s=1"><div style="margin-bottom: 2ex;">
                            <h2>Agregar Categoria</h2
                            <label><h6>Nombre</h6>
                            <input name="nombre" style="width: 85%;" type="text" placeholder="Escriba un nombre..." required></label>
                            </div>     
                            <input type="submit" class="success button" value="Agregar">
                            </form>