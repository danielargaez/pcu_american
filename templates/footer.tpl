

			<!-- Footer -->
				<footer id="footer">
					<ul class="icons">
						<li><a href="#" class="icon alt" style="font-size: 40px;color:white;"><span class="fab fa-facebook-square"></span></a></li>
						<li><a href="#" class="icon alt" style="font-size: 40px;color:white;"><span class="fab fa-twitter"></span></a></li>
						<li><a href="#" class="icon alt" style="font-size: 40px;color:white;"><span class="fas fa-envelope"></span></a></li>
					</ul>
					<ul class="copyright">
						<li>&copy; Castillo & Asociados. All rights reserved.</li>
					</ul>
				</footer>

		</div>
			<script src="/media/js/jquery.min.js"></script>
			<script src="/media/js/jquery.scrolly.min.js"></script>
			<script src="/media/js/jquery.dropotron.min.js"></script>
			<script src="/media/js/jquery.scrollex.min.js"></script>
			<script src="/media/js/browser.min.js"></script>
			<script src="/media/js/breakpoints.min.js"></script>
			<script src="/media/js/util.js"></script>
			<script src="/media/js/main.js"></script>
</body></html>