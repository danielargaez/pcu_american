<div class="content">
	{if $hayPosts}
		{foreach key=id item=post from=$busqueda}
			<article class="post excerpt">
            <div class="post-date-ribbon"><div class="corner"></div>{$post.fecha}</div>
            <header>                        
                <h2 class="title">
                    <a href="/posts/{$post.id}/{$post.categoriau}/{$post.titulou}.html" title="{$post.titulo}" rel="bookmark">{$post.titulo}</a>
                </h2>
                <div class="post-info">
                    <span class="theauthor"><span><i class="fi-eye"></i></span>{$post.visitas} Visitas</span>
                    <span class="featured-cat"><span><i class="ribbon-icon icon-bookmark"></i></span>{$post.categoria}</span>
                    <span class="thecomment"><span><i class="ribbon-icon icon-comment"></i></span><a href="/posts/{$post.id}/{$post.categoriau}/{$post.titulou}.html">Responder</a></span>
                </div>
            </header><!--.header-->
                    <a href="/posts/{$post.id}/{$post.categoriau}/{$post.titulou}.html" title="{$post.titulo}" id="featured-thumbnail">
                        <div class="featured-thumbnail">
                            <img width="150" height="150" src="{$post.thumbail}" class="attachment-ribbon-lite-featured size-ribbon-lite-featured wp-post-image" alt="" title="" sizes="(max-width: 150px) 100vw, 150px">                                                    </div>
                    </a>
                    <div class="post-content">
                    	{$post.contenido}            
                    </div>
             <div class="readMore"><a href="/posts/{$post.id}/{$post.categoriau}/{$post.titulou}.html" title="{$post.titulo}">Seguir Leyendo</a>
        </div>
</article>
		{/foreach}

		{$paginado}
	{else}
		<h2>Sin resultados</h2><h6>No hay publicaciones que mostrar.</h6>
	{/if}
</div>