<!DOCTYPE html>
<html >
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>{$pagename|html_entity_decode}</title>
{if $enPost}
<meta name="description" content=""/>
<link rel="canonical" href="{$seoUrl}" />
<meta property="og:locale" content="es_ES" />
<meta property="og:type" content="article" />
<meta property="og:title" content="{$seoTitulo} - Radio TV Peninsula Yucatan" />
<meta property="og:description" content="{$seoDescripcion}" />
<meta property="og:url" content="{$seoUrl}" />
<meta property="og:site_name" content="Radio TV Peninsula Yucatan" />
<meta property="og:image" content="{$seoImagen}" />
<meta property="og:image:width" content="1024" />
<meta property="og:image:height" content="682" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:description" content="{$seoDescripcion}" />
<meta name="twitter:title" content="{$seoTitulo} - Radio TV Peninsula Yucatan" />
<meta name="twitter:image" content="{$seoImagen}" />
{else}
<meta name="description" content="{$pagename} (c) 2018">
<meta name="keywords" content="Radio en vivo, TV, Noticias, Yucatan, Nacional, Internacional, Deportes, Salud, Espectaculo, Tecnologia">
{/if}
<meta name="author" content="Radio TV Peninsula Yucatan">
<base href="{$alterno}/">
<script src="http://code.jquery.com/jquery-2.0.3.js"></script>
<script src="/media/js/rtvpy/jquery.vticker.min.js"></script>
<link rel="alternate" href="/rss/" title="{$pagename} RSS Feed" type="application/rss+xml" />
<link rel="stylesheet" href="/media/css/rtvpy/foundation.css">
<link rel="stylesheet" href="/media/css/rtvpy/foundation-icons.css">
<link rel="stylesheet" href="/media/css/rtvpy/style.css">
<link rel="stylesheet" id="theme-slug-fonts-css" href="//fonts.googleapis.com/css?family=Monda%3A400%2C700" type="text/css" media="all">
<link rel="stylesheet" href="/media/css/rtvpy/rtl.css"><!-- fav -->
<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
<link rel="manifest" href="/favicon/site.webmanifest">
<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#2b5797">
<meta name="theme-color" content="#ffffff">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
  {literal}
<style>
body{
  background: url(/fondo-r2.jpg) repeat fixed;
}
@media only screen and (min-width: 768px) {
	.logo-resp
	{
    	margin-left: -90px;
    	margin-top: 20px;
	}

	.sticky {
	  position: fixed;
	  top: 0;
	  width: 100%
	}
	.stickyd{
	    position: fixed;
	    top: 24px;
	    right: 0;
	    z-index: 1;
	}
}
 #vzads{display:none !important}
.dropdown.menu > li.is-dropdown-submenu-parent > a::after {border-color: #ffffff transparent transparent;}
#page {
    padding-top: 30px;
    min-height: 100px;
    float: none;
    clear: both;
    overflow: hidden;
}

#site-header {
    float: none;
}
.widget-header {
    float: right;
    max-width: 600px;
    width: 100%;
    margin-top: 0;
}
#navigation ul ul-demo {
    position: absolute;
    width: 190px;
    padding: 20px 0 0 0;
    padding-top: 0px;
    z-index: 400;
    font-size: 12px;
    color: #798189;
    border: 1px solid #ccc;
    margin: 0;
    display:block;
}
</style>
{/literal}
<!---
<script type="text/javascript" src="/media/js/rtvpy/snowstorm.js"></script><script>
snowStorm.snowColor = "#2fc166";   // blue-ish snow!?
snowStorm.useTwinkleEffect = true; // let the snow flicker in and out of view
snowStorm.zIndex = 100;
snowStorm.followMouse = false;
snowStorm.vMaxX = 2;                 // Maximum X velocity range for snow
snowStorm.vMaxY = 2; 
</script>
--->
</head><body>
    <div class="main-container">
    <a class="skip-link screen-reader-text" href="#content">asdsa</a>
    <header id="site-header" role="banner" style="background: #ffffff;">
      <div class="container clear">
        <div class="site-branding logo-resp">
        <a onclick="verTransm(); return false;" class="button alert" style="float: right;right: 0;font-size: 10px;position: absolute;border-radius: 10px;">EN VIVO</a>
                                    <h1 id="logo" class="image-logo" style="max-width: 408px;max-height: 150px;" itemprop="headline">
                <a href="/" class="custom-logo-link" rel="home" itemprop="url"><img src="/logo-rtvpy.png" style="width: 100%;height: 100%;margin-top: -38px;" class="custom-logo" alt="{$pagename}" itemprop="logo"></a>      
                </h1><!-- END #logo -->       
        <div style="color:black;"><i class="fi-calendar"></i> {$fechita}</div>
                              </div><!-- .site-branding -->

        <ul class="menu edb" style="background: #364956;
    box-shadow: 0px 0px 1px 0px black;
    float: right;
    top: 8px;">

    {if $logeado}
    	<li style="background: #57cc308c;"><a href="/index.php?action=post"><i class="fi-plus"></i> Publicar</a></li>
    {/if}

    {if $logeado}        
      <li><a href="/admin"><i class="fi-widget"></i>  Administracion</a></li>
      <li><a href="/index.php?action=salir"><i class="fi-exit"></i>  Salir</a></li>
    {/if}
</ul>
        <div id="text-5" class="widget-header">     
        <div class="textwidget single_post" style="margin-top:10px;background-color: #fff0;
    padding: 20px 25px;
    float: left;
    width: 100%;
    border:none;
    box-sizing: border-box;">
{literal}
<style>

textwidget a:hover {
    color: #fff!important;
}.edb li a{
     font-family: 'Monda', sans-serif;
          color:white;
        }
    </style>
    {/literal}
<div id="publicSD" style="height:149px;display:none;">
<ul style="list-style:none;height:149px;">
	{foreach key=i item=ad from=$anunciostop}
	    <li style="height:149px;"><img src="{$ad}" style="background: #00000057;
	    box-shadow: 0px 0px 1px 0px white;
	    padding: 2px;
	    border-radius: 5px;width:745px;height:140px;"></li>
    {/foreach}
</ul>
</div>
<img src="https://placehold.it/745x140&text=745x140" style="display:none;background: #00000057;
    box-shadow: 0px 0px 1px 0px white;
    padding: 2px;
    border-radius: 5px;"></div>
    </div>     
    <div id="stickyscrolld" class="widget-header"> 
    <ul class="menu edb" style="font-size: 20px;
    float: right;
    right: 0;
    position: absolute;
    bottom: -22px;">
    <li style="background: #364956;box-shadow: 0px 0px 1px 0px black;"><a  style="cursor: pointer;" href="https://www.facebook.com/Radio-TV-Pen%C3%ADnsula-Yucat%C3%A1n-1982370352092725/" data-tooltip tabindex="1" title="Facebook" target="_blank"><span class="fi-social-facebook"></span></a></li>
    <li style="background: #364956;box-shadow: 0px 0px 1px 0px black;"><a href="https://play.google.com/store/apps/details?id=net.duckapps.radiotvpeninsulayucatan" style="cursor: pointer;" data-tooltip tabindex="2" title="Aplicacion Android" target="_blank"><span class="fi-social-android"></span></a></li>
    <li style="background: #364956;box-shadow: 0px 0px 1px 0px black;"><a href="/rss/" style="cursor: pointer;" data-tooltip tabindex="3" title="RSS"><span class="fi-rss" style="color: #ff8100;"></span></a></li>

    </div>
</ul>
    </div>
      <div class="primary-navigation">
      <span data-responsive-toggle="navigation" >
        <a id="pull" class="toggle-mobile-menu" data-toggle="navigation">Menu</a></span>
        <div class="container clear" id="stickyscroll" style="    max-width: 100%;">
          <nav id="navigation" class="primary-navigation mobile-menu-wrapper" role="navigation">
                          <ul id="menu-navigation" class="dropdown menu" data-dropdown-menu>
    <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-1305"><a href="/"><i class="fi-home"></i> Inicio</a></li>
  {foreach key=i item=cat from=$categorias}
    <li><a href="/categoria/{$cat.id}/{$cat.nombreu}" data-tooltip data-position="top" style="cursor: pointer;" title="{$cat.nombre}">{$cat.nombre}</a></li>
  {/foreach}
  


                          
</ul>                     <div id="mobile-menu-overlay"></div></nav>



<!-- #site-navigation -->
        </div>
      <div id="mobile-menu-overlay"></div></div>
    </header>
{literal}
    <style>
#overlay {
    position: fixed;
    display: none;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(0, 0, 0, 0.75);
    font-family: 'Monda', sans-serif;
    z-index: 117;
}

#text{
    position: absolute;
    font-family: 'Monda', sans-serif;
    top: 50%;
    left: 50%;
    font-size: 50px;
    color: white;
    transform: translate(-50%,-50%);
    -ms-transform: translate(-50%,-50%);
}
</style>
{/literal}
</head>
<body>

<div id="overlay">
  <div id="text">
  <center onclick="off()" style="font-size: 13px;
    cursor: pointer;
    float: right;
    margin-top: -39px;"><i class="fi fi-x"></i> Cerrar</center>
<iframe
  style="border: 1px solid white;box-shadow: 0px 0px 0px 3px black;" 
  width="700"
  height="600"
  src="https://www.youtube.com/embed/live_stream?channel=UCdOKGhySFhKTVHeKeAzWs8Q"
  frameborder="0"
  allowfullscreen>
  Cargando...
</iframe>
  </div>
</div>
{literal}
<script>
function verTransm() {
    document.getElementById("overlay").style.display = "block";
}

function off() {
    document.getElementById("overlay").style.display = "none";
    location="/";
}

</script>
{/literal}

<div id="page" class="home-page">
  <div class="content">
    <article class="article">
      <div id="content_box">
<div id="fb-root"></div>
{literal}
<script>
$(function() {
  $('#publicSD').vTicker('init', {speed: 400, 
    pause: 4000,
         animation:'fade',
         mousePause:true,
    showItems: 1,
    padding:4});

	window.onscroll = function() {mScroll()};
	var header = document.getElementById("stickyscroll");
	var headerd = document.getElementById("stickyscrolld");
	var sticky = header.offsetTop;
	var stickyd = headerd.offsetTop;
	function mScroll() {
	  if (window.pageYOffset > sticky) {
	    header.classList.add("sticky");
	    headerd.classList.add("stickyd");
	  } else {
	    header.classList.remove("sticky");
	    headerd.classList.remove("stickyd");
	  }
	}
});
  window.fbAsyncInit = function() {
    FB.init({
      appId            : '172639896738618',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v3.0'
    });
  };

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.0&appId=172639896738618&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KBPMDW8');</script>
<!-- End Google Tag Manager -->
{/literal}