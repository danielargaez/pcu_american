﻿</div>
      </div>
    </article>
<aside class="sidebar c-4-12">
  <div id="sidebars" class="sidebar">
    <div class="sidebar_list">
<!--- Nuevo Up --->
{if $logeado  && ($actionp == "publicidad" || $actionp == "post" || $actionp == "paginas")}
	{literal}
	<style>
	.subirimgs{
		cursor: move;border: 1px dashed #b5b5b5;
	}
	</style>
	<script>
	    $("document").ready(function () {
	        $('input[type=file]').on("change", function () {

	            var $files = $(this).get(0).files;

	            if ($files.length) {

	                // Reject big files
	                if ($files[0].size > $(this).data("max-size") * 1024) {
	                    alert("Archivo muy pesado!");
	                    return false;
	                }

	                // Replace ctrlq with your own API key
	                var apiUrl = 'https://api.imgur.com/3/image';
	                var apiKey = '7ec1189db39dd05';

	                var formData = new FormData();
	                formData.append("image", $files[0]);
	                var settings = {
	                    "async": true,
	                    "crossDomain": true,
	                    "url": apiUrl,
	                    "method": "POST",
	                    "datatype": "json",
	                    "headers": {
	                        "Authorization": "Client-ID " + apiKey
	                    },
	                    "processData": false,
	                    "contentType": false,
	                    "data": formData,
	                    beforeSend: function (xhr) {                        
							$('#server-results').html("Subiendo Foto...");
	                    },
	                    success: function (res) {
	                        console.log(res.data.link);
	                        $('#server-results').html("");
	                        $('#server-results').append('<b>Enlace de la imagen:</b><br><input onfocus="this.select();" onmouseup="return false;" type="text" value="' + res.data.link + '"><b>Vista Previa:</b><br>');
	                        $('#server-results').append('<img class="subirimgs" src="' + res.data.link + '" />');
	                        $('#server-results').append('<br><span style="font-size:10px;"><b>Tip:</b> Se puede arrastrar la imagen directamente hacia el cuadro de texto.</span>');
	                    },
	                    error: function () {
							$('#server-results').html("Error al subir la imagen.");
	                    }
	                }
	                $.ajax(settings).done(function (response) {
	                    console.log("Done");
	                });
	            }
	        });
	    });
	</script>
	{/literal}
	<div id="subir-imgs" style="background: white;
	    border-radius: 9px;
	    border: 1px solid #ccc;
	    padding: 9px;" class="widget widget_recent_entries">    
	<h3 class="widget-title">Subir Archivo</h3> 
	<form id="imgur">
	    <input type="file" class="imgur" accept="image/*" data-max-size="5000" />
	</form>
	  <div id="server-results"></div>

	</div>
{/if}

<!--- player --->
<script type="text/javascript" src="/jplayer/jplayer/jquery.jplayer.min.js"></script>
<link href="/jplayer/skin/blue.monday/css/jplayer.blue.monday.min.css" rel="stylesheet" type="text/css" />
{literal}
<style>
.jp-audio-stream {
    width: 100%;
    padding: 22px 19px 11px;
}.jp-audio-stream .jp-controls {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
}.jp-audio .jp-interface, .jp-audio-stream .jp-interface {
    height: 51px;
}
.jp-audio-stream .jp-volume-controls {
    left: 57px;
    z-index: 9;
}.jp-volume-max {
    left: 181px;
}
.jp-volume-bar {
    position: absolute;
    overflow: hidden;
    top: 5px;
    left: 22px;
    width: 150px;
}
.jp-volume-controls {
    position: absolute;
    top: 11px;
}
.jp-details .jp-title {
    margin: 0;
    padding: 5px 0px;
    font-size: .72em;
    font-weight: 700;
}
*:focus {
  outline: none;
}

#project-wrapper {
  height: auto;
  display: flex;
  display: -webkit-flex;
  -webkit-align-items: center;
  align-items: center;
  -webkit-justify-content: center;
  justify-content: center;
}
body #project-wrapper #project-container {
  background: url("http://www.upsetmagazine.com/wp-content/uploads/2015/09/Bring-Me-The-Horizon_16.jpg");
  background-size: cover;
  background-position: top center;
  color: white;
  /* height: auto; */
  height: 180px;
  min-width: 300px;
  width: 300px;
  position: relative;
  z-index: 2;
  border-radius: 5px;
  overflow: hidden;
  -webkit-box-shadow: 0px 3px 15px -1px rgba(0, 0, 0, 0.5);
  -moz-box-shadow: 0px 3px 15px -1px rgba(0, 0, 0, 0.5);
  box-shadow: 0px 3px 15px -1px rgba(0, 0, 0, 0.5);
}
body #project-wrapper #project-container #overlay {
  position: absolute;
  height: calc(100% + 1px);
  width: calc(100% + 1px);
  z-index: 2;
  background: -moz-linear-gradient(-45deg, #333333 0%, #222222 100%);
  /* FF3.6-15 */
  background: -webkit-linear-gradient(-45deg, #333333 0%, #222222 100%);
  /* Chrome10-25,Safari5.1-6 */
  background: linear-gradient(135deg, #333333 0%, #222222 100%);
  /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
  opacity: .85;
}
body #project-wrapper #project-container #content {
  text-align: center;
  position: relative;
  z-index: 3;
}
body #project-wrapper #project-container #content h2 {
  font-size: 1.3em;
  margin: 35px 0 0 0;
  font-weight: 600;
}
body #project-wrapper #project-container #content h3 {
  margin: 7px 0 15px 0;
  font-size: 0.9em;
  opacity: .4;
}
body #project-wrapper #project-container #content input {
  width: 100%;
  height: 2px;
  margin: 0 0 15px 0;
  background: -moz-linear-gradient(left, #3498db 0%, #9b59b6 72%);
  /* FF3.6-15 */
  background: -webkit-linear-gradient(left, #3498db 0%, #9b59b6 72%);
  /* Chrome10-25,Safari5.1-6 */
  background: linear-gradient(to right, #3498db 0%, #9b59b6 72%);
  /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
}
body #project-wrapper #project-container #content input::-webkit-slider-thumb {
  -webkit-appearance: none;
  height: 8px;
  width: 8px;
  border-radius: 10px;
  background: #ffffff;
  cursor: pointer;
}
body #project-wrapper #project-container #content #controls {
  height: 50px;
  display: flex;
  display: -webkit-flex;
  -webkit-align-items: center;
  align-items: center;
  -webkit-justify-content: center;
  justify-content: center;
}
body #project-wrapper #project-container #content #controls .column {
  float: left;
}
body #project-wrapper #project-container #content #controls .column:nth-child(even) {
  font-size: 1.3em;
}
body #project-wrapper #project-container #content #controls .column:nth-child(n+2):nth-child(-n+4) {
  color: white !important;
}
body #project-wrapper #project-container #content #controls .column:nth-child(3) {
  font-size: 2em;
}
body #project-wrapper #project-container #content #controls .column.active {
  color: #9b59b6;
}
body #project-wrapper #project-container #content #controls .column i {
  margin: 0 15px;
  cursor: pointer;
}
body #dailyui {
  position: fixed;
  font-size: 12em;
  font-weight: 700;
  margin: 0 0 -28px 0;
  padding: 0;
  right: 0;
  bottom: 0;
  color: rgba(0, 0, 0, 0.3);
  z-index: 1;
  text-align: right;
  font-family: 'proxima-nova', 'Lato', sans-serif;
}

</style>
<script type="text/javascript">
$(document).ready(function(){

var stream = {
  {/literal}
    title: '<marquee width="100%" scrolldelay="100" scrollamount="5" direction="left" loop="infinite">{$currentsong}</marquee>',
    mp3: "http://www.radiotvpeninsulayucatan.com:8000/stream"
    {literal}
  },
  ready = false;
 $("#jquery_jplayer_1").jPlayer({
    ready: function (event) {
      ready = true;
      $(this).jPlayer("setMedia", stream);
    },
    pause: function() {
      $(this).jPlayer("clearMedia");
    },
    error: function(event) {
      if(ready && event.jPlayer.error.type === $.jPlayer.error.URL_NOT_SET) {
        // Setup the media stream again and play it.
        $(this).jPlayer("setMedia", stream).jPlayer("play");
      }
    },
    swfPath: "../dist/jplayer",
    supplied: "mp3",
    preload: "none",
    wmode: "window",
    useStateClassSkin: true,
    autoBlur: false,
    keyEnabled: true
  });
});
</script>
<script>

var mT = setInterval(obData, 60000);

function obData() {
  var showData = $('.jp-title');
      showData.text("Obteniendo Información...");
      $.ajax({
          type: 'GET',
          url: '/gjson.php',
          success: function(data) {
              showData.html(data);
          }
      });
}

function myStopFunction() {
    clearInterval(mT);
}

$(document).ready(function () {
  $('#get-data').click(function () {
      obData();
  });
    $('.jp-play').click(function () {
      obData();
  });
  
});

</script>
{/literal}
    <div id="social-profile-icons-2" class="widget social-profile-icons">    

<div id="project-wrapper">
	<div id="project-container">
		<div id="overlay"></div>
		<div id="content">
			<h2>Throne</h2>
			<h3>Bring Me The Horizon</h3>
			<input type="range" value="30" />
			<div id="controls">
				<div class="column"><i class="fi-refresh" aria-hidden="true"></i></div>
				<div class="column"><i class="fi-step-backward" aria-hidden="true"></i></div>
				<div class="column"><i class="fi-play play-btn fa-fw" aria-hidden="true"></i></div>
				<div class="column"><i class="fi-step-forward" aria-hidden="true"></i></div>
				<div class="column active"><i class="fa fa-random" aria-hidden="true"></i></div>
			</div>
		</div>
	</div>
</div>

<div id="jquery_jplayer_1" class="jp-jplayer"></div>
<div id="jp_container_1" class="jp-audio-stream" role="application" aria-label="media player">
<center><p>Radio TV Peninsula Yucatán<br><span style="font-size: 12px;">
{if $radiostatus}
	<font color=green><b>ONLINE</b> </font><br />
{else if}
	<font color=red><b>OFFLINE</b></font><br />
{/if}
</span></p></center>
  
  {if $radiostatus}
    <div class="jp-type-single">
    <div class="jp-gui jp-interface">
      <div class="jp-volume-controls">
        <button class="jp-mute" role="button" tabindex="0">mute</button>
        <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
        <div class="jp-volume-bar">
          <div class="jp-volume-bar-value"></div>
        </div>
      </div>
      <div class="jp-controls">
        <button class="jp-play" role="button" tabindex="0">play</button>
      </div>
    </div>
    <div class="eahora" style="font-size: 10px;text-align:  left;"><b><a id="get-data"><i class="fi-refresh"></i> Sonando Ahora:</a></b></div>
    <div id="show-data"></div>
    <div class="jp-details">
      <div class="jp-title" aria-label="title">{$currentsong}&nbsp;</div>
    </div>
  {/if}
    
    <div class="jp-no-solution">
      <audio controls="controls" preload="auto" style="width: 100%;">
      <source src="http://162.248.91.166:8000/stream" /> <!-- origen para FF/Opera/Chrome -->
      <!-- cargar flash para IE6/7/8 -->
      <object type="application/x-shockwave-flash" data="/pf.swf" width="10" height="10">
      <param name="movie" value="/pf.swf" />
      <param name="loop" value="false" /> 
      <param name="menu" value="false" />
      <param name="play" value="true" />
      <param name="autoplay" value="true" />
      <param name="bgcolor" value="#ffffff" />
      <p>Flash Palyer no se encuentra o la versión no es compatible, utiliza el icono para ir a la página de descarga <br />
      <a href="http://get.adobe.com/es/flashplayer/" onclick="this.target='_blank'">Descargar Flash Player</a>
      </p>
      </object>
      </audio>
    </div>
  </div>
</div>


    </div>   

      <div id="search-3" class="widget widget_search"><form method="get" id="searchform" class="search-form" action="/" _lpchecked="1">
  <fieldset>
    <input type="text" name="s" id="s" placeholder="Escribe algo..." >
    <input type="submit" value="Buscar">
  </fieldset>
</form>
</div>


{foreach key=isd item=ns from=$anunciosderechaex}
  {if $video}
    <iframe width="300" height="250" src="{$ns.url}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
  {else}
    <div id="text-8" class="widget widget_text"><div class="textwidget"><p><img src="{$ns.url}" style="width:300px;height:250px;"></p></div>
  {/if}
{/foreach}
    </div>

    <div id="recent-posts-3" class="widget widget_recent_entries">
    	<h3 class="widget-title">Ultimas Publicaciones</h3>
    	{foreach key=recs item=post from=$recientes}
    		<li><a href="/posts/{$post.id}/{$post.categoria}/{$post.titulou}.html">{$post.titulo}</a></li>
    	{/foreach} 
    </div>
    <div id="text-2" class="widget widget_text"><h3 class="widget-title">{$pagename}</h3>     
    <div class="textwidget">{$descSitio}</div>
    </div>


    </div>
  </div><!--sidebars-->
</aside>

</div>
</div>

</article>
<div id="sfc1qd65b6pxuktp5kulq6h2feyfrend44p"></div><script type="text/javascript" src="https://counter5.wheredoyoucomefrom.ovh/private/counter.js?c=1qd65b6pxuktp5kulq6h2feyfrend44p&down=async" async></script><noscript><a href="https://www.contadorvisitasgratis.com" title="contadores de visitas"><img src="https://counter5.wheredoyoucomefrom.ovh/private/contadorvisitasgratis.php?c=1qd65b6pxuktp5kulq6h2feyfrend44p" border="0" title="contadores de visitas" alt="contadores de visitas"></a></noscript>

<footer id="site-footer" role="contentinfo">
    <!--start copyrights-->
<div class="copyrights">
    <div class="container">
        <div class="row" id="copyright-note">
            <span><a href="/" title="'.$page_name.'">{$pagename}</a> &copy; 2019 Todos los derechos reservados.</span>
            <div class="top">
                Desarrollado por <a href="/duckapps/" title="DuckApps Dev">DuckApps</a> con <a href="/duckapps/">DuckCMS</a>
            </div>
        </div>
    </div>
</div>
<!--end copyrights-->
  </footer>
<script src="/media/js/rtvpy/vendor/foundation.js"></script>
    {literal}
    <script>
      $(document).foundation();
    </script>
<script>
	$( document ).ready(function() {
	    $("#publicSD").fadeIn("fade");
	});
</script>
{/literal}
    </body></html>
