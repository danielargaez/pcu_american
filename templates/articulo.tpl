<style type="text/css">
  /* Position the image container (needed to position the left and right arrows) */
.containerG {
  position: relative;
}

/* Hide the images by default */
.mySlides {
  display: none;
}

/* Add a pointer when hovering over the thumbnail images */
.cursor {
  cursor: pointer;
}

/* Next & previous buttons */
.prev,
.next {
  cursor: pointer;
  position: absolute;
  top: 40%;
  width: auto;
  padding: 16px;
  margin-top: -50px;
  color: white;
  font-weight: bold;
  font-size: 20px;
  border-radius: 0 3px 3px 0;
  user-select: none;
  -webkit-user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover,
.next:hover {
  background-color: rgba(0, 0, 0, 0.8);
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* Container for image text */
.caption-container {
  text-align: center;
  background-color: #222;
  padding: 2px 16px;
  color: white;
}

.rowG:after {
  content: "";
  display: table;
  clear: both;
}

/* Six columns side by side */
.column {
  float: left;
  width: 16.66%;
}

/* Add a transparency effect for thumnbail images */
.demo {
  opacity: 0.6;
}

.active,
.demo:hover {
  opacity: 1;
}
</style>

<div class="container" style="margin-top:100px;">
            
            <div class="row gtr-150">
              <div class="col-8 col-12-medium">

                <section style="margin-bottom: 20px;">
                  <div class="containerG">
                    <!-- Full-width images with number text -->
                    {$galeria_s}
                    
                    <!-- Next and previous buttons -->
                    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                    <a class="next" onclick="plusSlides(1)">&#10095;</a>

                    <!-- Thumbnail images -->
                    <div class="rowG">
                      {$galeria}
                    </div>
                  </div>
                </section>
                <!-- Content -->
                  <section id="content">      
                    <h3 style="margin-bottom: 8px;">Descripcion:</h3>              
                    {$contenido}
                  </section>

              </div>
              <div class="col-4 col-12-medium">

                <!-- Sidebar -->
                  <section id="sidebar">
                    <section style="display: none">
                      <h3>Ubicacion</h3>
                      <iframe width="350" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-89.77237701416017%2C20.859774252630768%2C-89.48398590087892%2C21.08802375534542&amp;layer=mapnik&amp;marker=20.973942574596638%2C-89.62818145751953" style="border: 1px solid black"></iframe>
                    </section>
                    <hr>
                    <section>
                      <a href="#" class="image fit"><img src="images/pic07.jpg" alt=""></a>
                      <h3>Contacto</h3>  
                        <form action="/create_property_request" accept-charset="UTF-8" data-remote="true" method="post"><input name="utf8" type="hidden" value="✓">
                          <div class="form-group">
                            <input class="form-control" placeholder="Tu nombre" type="text" name="contact_request[name]" id="contact_request_name">
                          </div>
                          <div class="form-group">
                            <input class="form-control" placeholder="Email" type="text" name="contact_request[email]" id="contact_request_email">
                          </div>
                          <div class="form-group">
                            <input class="form-control" placeholder="Número de teléfono" type="text" name="contact_request[phone_number]" id="contact_request_phone_number">
                          </div>
                          <div class="form-group">
                            <textarea rows="4" class="form-control" name="contact_request[message]" id="contact_request_message">Me interesa mucho esta propiedad y quiero recibir más información.
                      ¡Gracias!</textarea>    </div>
                          <div class="form-group">
                            <input type="submit" name="commit" value="Enviar" id="submit_button" data-submit-with-text="Enviando..." class="btn submit btn-primary btn-lg btn-block" autocomplete="off">
                          </div>
                      </form>
                    </section>
                  </section>

              </div>
            </div>
          </div>


<script src="/galeria.js"></script>
 <script src="//platform-api.sharethis.com/js/sharethis.js#property=5b36a346c5ed960011521534&product=inline-share-buttons"></script>

{if $logeado}
<div class="reveal" id="animatedModal10" data-reveal data-close-on-click="true" data-animation-in="spin-in" data-animation-out="spin-out">
  <h1>¿Seguro que desea borrar el articulo?</h1>
  <center><a href="/index.php?action=admin&p=post&b={$post.idPost}" class="button alert" >Borrar</a>
  <a data-close  class="button" >No</a>
  </center>
  <button class="close-button" data-close aria-label="Close reveal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
{/if}

                        {if $logeado}
                          <div class="post-date-ribbon" style="float:right;background:transparent;margin-top: -8px;"><a class="button success" href="/index.php?action=admin&p=post&e={$post.idPost}">Editar</a><a data-toggle="animatedModal10" class="button alert" >Borrar</a></div>
                        {/if}
