  
<style type="text/css">
  a.button {
    background: #202020;
    padding: 10px 20px;
    display: inline-block;
    margin-top: 0px;
    margin-bottom: 0px;
    color: #fff;
}

</style>
    <!--Sub Header Start-->  
    <article id="sub_bg">
        <div class="sub_header">
            <h2 class="thumb_img">{$PagTitulo}</h2>
            <p class="description">                            {if $logeado}
                                <div class="reveal" id="animatedModal10" style="display: none" data-reveal data-close-on-click="true" data-animation-in="spin-in" data-animation-out="spin-out">
                                  <h1>¿Seguro que desea borrar la pagina?</h1>
                                  <center><a href="/index.php?action=admin&p=paginas&b={$idPag}" class="button alert" >Borrar</a>
                                  <a data-close  class="button" >No</a>
                                  </center>
                                  <button class="close-button" data-close aria-label="Close reveal" type="button">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <a class="button success" href="/index.php?action=admin&p=paginas&e={$idPag}">Editar</a>
                                <a data-toggle="animatedModal10" class="button alert" >Borrar</a>
                            {/if}</p>
        </div>
    </article>
    <!--Sub Header End--> 

    <!--Content Start-->
    <article class="content" style="color:black;">
        {$PagContenido}
    </article>
    <!--Content End-->
         {if $PagComentarios != 0} <div class="related-posts"><div class="postauthor-top"><h3>Comentarios</h3></div> <div class="fb-comments" data-href="{$alterno}/index.php?action=pagina&i={$idPag}" data-width="668" data-numposts="10">Cargando...</div></div>
             {/if} 
