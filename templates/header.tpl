﻿<!DOCTYPE html>
<html >
  <head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
<title>{$pagename}</title>
<meta name="description" content="{$pagename} © 2018">
<meta name="keywords" content="">
<meta name="author" content="DuckApps Dev">
<base href="{$alterno}/"> 
<link rel="stylesheet" href="/media/css/main.css" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<noscript><link rel="stylesheet" href="/media/css/noscript.css" /></noscript>
<style>
#banner {
    background-attachment: fixed;
    background-color: #2c3355;
    background-image: url("http://castilloyasociadosmexico.com/logo-one.png");
    background-size: contain;
    background-repeat: no-repeat;
}

    a:hover {
      color: #5b69ab !important;
      border-bottom-color: transparent;
    }
.spotlight.style1 .content {
    border-color: #44508c;
}
header.major:after {
    background: #44508c;
    content: '';
    display: inline-block;
    height: 0.2em;
    max-width: 20em;
    width: 75%;
}
.wrapper.style2 {
    background: #44508c;
}
body.landing #header {
    background: #44508c7d;
    box-shadow: none;
    position: fixed;
    color: black;
}
.overflou{
  overflow: hidden;
}
</style>
</head>
  <body class="is-preload landing {if $action == ''}overflou{/if}">
    <div id="page-wrapper">

      <!-- Header -->
        <header id="header" {if $action == ''}style="display:none;"{/if}>
          <center><span id="logo" style="padding-top: 9px;"><a style="text-decoration: none; color:white; border: none;" href="/">Castillo & Asociados</a></span></center>
        </header>