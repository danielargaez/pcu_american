<!DOCTYPE html>
<html >
  <head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{$pagename} travler</title>
  <meta name="description" content="{$pagename} © 2018">
  <meta name="keywords" content="">
  <meta name="author" content="DuckApps Dev">
  <base href="{$alterno}/">
  <link rel="stylesheet" href="/media/css/main.css" />
  <noscript><link rel="stylesheet" href="/media/css/noscript.css" /></noscript>

  <!-- Facebook and Twitter integration -->
  <meta property="og:title" content=""/>
  <meta property="og:image" content=""/>
  <meta property="og:url" content=""/>
  <meta property="og:site_name" content=""/>
  <meta property="og:description" content=""/>
  <meta name="twitter:title" content="" />
  <meta name="twitter:image" content="" />
  <meta name="twitter:url" content="" />
  <meta name="twitter:card" content="" />

  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
  
  <!-- Animate.css -->
  <link rel="stylesheet" href="/media/css/animate.css">
  <!-- Icomoon Icon Fonts-->
  <link rel="stylesheet" href="/media/css/icomoon.css">
  <!-- Themify Icons-->
  <link rel="stylesheet" href="/media/css/themify-icons.css">
  <!-- Bootstrap  -->
  <link rel="stylesheet" href="/media/css/bootstrap.css">

  <!-- Magnific Popup -->
  <link rel="stylesheet" href="/media/css/magnific-popup.css">

  <!-- Magnific Popup -->
  <link rel="stylesheet" href="/media/css/bootstrap-datepicker.min.css">

  <!-- Owl Carousel  -->
  <link rel="stylesheet" href="/media/css/owl.carousel.min.css">
  <link rel="stylesheet" href="/media/css/owl.theme.default.min.css">

  <!-- Theme style  -->
  <link rel="stylesheet" href="/media/css/style.css">

  <!-- Modernizr JS -->
  <script src="/media/js/modernizr-2.6.2.min.js"></script>
  <!-- FOR IE9 below -->
  <!--[if lt IE 9]>
  <script src="/media/js/respond.min.js"></script>
  <![endif]-->

  </head><body>
    
  <div class="gtco-loader"></div>
  
  <div id="page">

  
  <!-- <div class="page-inner"> -->
  <nav class="gtco-nav" role="navigation">
    <div class="gtco-container">
      
      <div class="row">
        <div class="col-sm-4 col-xs-12">
          <div id="gtco-logo"><a href="index.html">{$pagename} <em>.</em></a></div>
        </div>
        <div class="col-xs-8 text-right menu-1">
          <ul>
            <li><a href="destination.html">Destination</a></li>
            <li class="has-dropdown">
              <a href="#">Travel</a>
              <ul class="dropdown">
                <li><a href="#">Europe</a></li>
                <li><a href="#">Asia</a></li>
                <li><a href="#">America</a></li>
                <li><a href="#">Canada</a></li>
              </ul>
            </li>
            <li><a href="pricing.html">Pricing</a></li>
            <li><a href="contact.html">Contact</a></li>
          </ul> 
        </div>
      </div>
      
    </div>
  </nav>  