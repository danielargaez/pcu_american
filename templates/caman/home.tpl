{if $enBusqueda}
<div class="content">
	{if $hayPosts}
		{foreach key=id item=post from=$busqueda}
			<div class="cell" style="background: #0f2b4d;border-radius: 6px;padding: 10px;color: white;">
			<a href="/posts/{$post.id}/{$post.categoria}/{$post.titulo}"><img class="thumbnail" style="width:220px;height:220px;" src="{$post.thumbail}"></a>
			<center><h6 style="color:white;">{$post.titulo}</h6>
				<a href="/posts/{$post.id}/{$post.categoria}/{$post.titulo}" class="button expanded">Ver Producto</a>
			</div>
		{/foreach}

		{$paginado}
	{else}
		<h2>Sin resultados</h2><h6>No hay publicaciones que mostrar.</h6>
	{/if}
	</div>

{else}
	<div id="slidecaption"></div>
	<ul id="slide-list"></ul>
{/if}