<html dir="ltr" lang="en-US">
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="initial-scale=1, maximum-scale=1" />
<meta name="viewport" content="width=device-width" />
<title>{$pagename}</title>
<link rel="stylesheet" type="text/css" media="screen" href="/media/css/caman/reset.css"/>
<link rel="stylesheet" type="text/css" media="screen" href="/media/css/caman/layout.css" />
<link rel="stylesheet" type="text/css" media="screen" href="/media/css/caman/menu.css" />
<link rel="stylesheet" type="text/css" media="screen" href="/media/css/caman/supersized.css" />
<link rel="stylesheet" type="text/css" media="screen" href="/media/css/caman/supersized.shutter.css" />
<link rel="stylesheet" type="text/css" media="screen" href="/media/css/caman/font-awesome.css" />
<link rel="stylesheet" type="text/css" media="screen" href="/media/css/caman/style.css" />
<link rel="stylesheet" type="text/css" media="screen" href="/media/css/caman/skin.css" />
<link rel="stylesheet" type="text/css" media="screen" href="/media/css/caman/responsive.css" />
<link rel="stylesheet" type="text/css" media="screen" href="/media/css/bootstrap.css" />
<link href="http://fonts.googleapis.com/css?family=Oswald:400,300,600,700,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
</head>

<style>
figure {
    text-align: center;
    background-image: url(/media/images/caman/logo_bg.png);
    background-repeat: repeat-x;
    background-color: #0000006b;
    overflow: hidden;
    padding: 0px;
    margin-bottom: 10px;
    border-radius: 13px;
}
a.active {
    background: #f3c792;
    color: #000!important;
    position: relative;
    background-image: url(/media/images/caman/menu_bg_hover.png);
    background-position: right;
    background-repeat: no-repeat;
}
.menu li a:hover{
    background-image:url(/media/images/caman/menu_bg_hover.png);
    background-repeat:no-repeat;
    background-color:bisque;
    color:#000!important;
    background-position:right;  
}

#page_wrapper {
    width: 1234px;
    overflow: hidden;
    padding-top: 5%;
    margin-left: 25px;
    margin-bottom: 30px;
    display: block;
}
.rightside {
    float: right;
    width: 900px;
}
#slidecaption h1 {
    font-size: 80px;
    text-transform:uppercase;
    margin-bottom:30px;
    color: #FFFFFF !important;
    line-height:110%;
    text-shadow: 5px 5px 5px #00000069;
}
#slidecaption h4 {
    color: #FFFFFF;
    font-size:20px;
    padding:10px 0px;
    text-transform:uppercase;
    border-top:5px solid rgba(0,0,0,0.3);
    border-bottom:5px solid rgba(0,0,0,0.3);
    text-shadow: 5px 5px 5px #00000069;
    display:table;
}
</style>

<body>
<div id="dvLoading"></div>
<div id="bgPattern">&nbsp;</div>

<!--Page Wrapper Start-->
<div id="page_wrapper">

<div class="leftside">
    <!--Menu Logo Section Start-->
    <figure><a href="index.html"><img src="/media/images/caman/logo.png" alt="img" /></a></figure>
    <a id="resp-menu" class="responsive-menu" href="#"><i class="fa fa-reorder"></i> Menu</a>
    <nav>
        <ul class="menu">
            <li><a class="active" href="/">Inicio</a></li>
            <li><a href="/2/sobre-mi.html">Sobre Mí</a></li>
            <li><a href="/blog/1">Belleza</a></li>
            <li><a href="/blog/2">Estilo de Vida</a></li>
            <li><a href="/blog/3">Moda</a></li>
            <li><a href="/portafolio">Portafolio</a></li>
            <li><a href="/video">Video</a></li>
        </ul>
    </nav>
    <!--Menu Logo Section End-->
    <div class="panel_social_icons">
        <ul>
            <li><a href="#"><img src="/media/images/caman/facebook.png" alt="img"/></a></li>
            <li><a href="#"><img src="/media/images/caman/skype-icon.png" alt="img"/></a></li>
            <li><a href="#"><img src="/media/images/caman/stumbleupon-icon.png" alt="img"/></a></li>
            <li><a href="#"><img src="/media/images/caman/twitter.png" alt="img"/></a></li>
            <li><a href="#"><img src="/media/images/caman/vimeo-icon.png" alt="img"/></a></li>
        </ul>
    </div>
    <div class="copyrights" style="color:#fff;">&copy; {$pagename}</div>
</div>

<div class="rightside">