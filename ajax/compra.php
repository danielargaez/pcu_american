<?php 
        include("../plantillas/config.php");
   // Obtenemos los datos en formato variable1=valor1&variable2=valor2&...
        $raw_post_data = file_get_contents('php://input');

        // Los separamos en un array
        $raw_post_array = explode('&',$raw_post_data);

        // Separamos cada uno en un array de variable y valor
        $myPost = array();
        foreach($raw_post_array as $keyval){
            $keyval = explode("=",$keyval);
            if(count($keyval) == 2)
                $myPost[$keyval[0]] = urldecode($keyval[1]);
        }

        // Nuestro string debe comenzar con cmd=_notify-validate
        $req = 'cmd=_notify-validate';
        if(function_exists('get_magic_quotes_gpc')){
            $get_magic_quotes_exists = true;
        }
        foreach($myPost as $key => $value){
            // Cada valor se trata con urlencode para poder pasarlo por GET
            if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            } else {
                $value = urlencode($value);
            }

            //Añadimos cada variable y cada valor
            $req .= "&$key=$value";
        }
     
    // STEP 2: POST IPN data back to PayPal to validate
    $ch = curl_init('https://www.sandbox.paypal.com/cgi-bin/webscr');
    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
    // In wamp-like environments that do not come bundled with root authority certificates,
    // please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set 
    // the directory path of the certificate as shown below:
    // curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
    if( !($res = curl_exec($ch)) ) {
        error_log("Got " . curl_error($ch) . " when processing IPN data");
        curl_close($ch);
        exit;
    }
    curl_close($ch);
     
    if (strcmp ($res, "VERIFIED") == 0) {
        $item_number = $_POST['item_number'];
        $payment_status = $_POST['payment_status'];
        $payment_amount = $_POST['mc_gross'];
        $payment_currency = $_POST['mc_currency'];
        $txn_id = $_POST['txn_id'];
        $receiver_email = $_POST['receiver_email'];
        $payer_email = $_POST['payer_email'];
        $payment_date = $_POST['payment_date'];
        foreach($_POST as $key => $value) {
          echo $key." = ". $value."<br>";
        }

        //Checamos si ya existe o no!
        $sql = "SELECT pTxn FROM pedidos WHERE pTxn = '$txn_id'";
        $result = mysqli_query($conn, $sql);
        $contarTran = mysqli_num_rows($result);

        if($payment_status == "Completed")
        {
            if ($contarTran > 0)
            {
                $sql =  "UPDATE pedidos SET pStatus = '$payment_status', pFechaCompleto = '$payment_date' WHERE pTxn = '$txn_id'";
                mysqli_query($conn, $sql);
            }
            else
            {
                $sql =  "INSERT INTO pedidos(pStatus, pPrecio, pCurrency, pTxn, pReceiver, pPayer, pFechaCreado) VALUES('$payment_status','$payment_amount','$payment_currency','$txn_id','$receiver_email','$payer_email','$payment_date')";
                mysqli_query($conn, $sql);
            }
        }
        else
        {
           
            if ($contarTran > 0)
            {
                $sql =  "UPDATE pedidos SET pStatus = '$payment_status' WHERE pTxn = '$txn_id'";
                mysqli_query($conn, $sql);
            }
            else{
                $sql =  "INSERT INTO pedidos(pStatus, pPrecio, pCurrency, pTxn, pReceiver, pPayer, pFechaCreado) VALUES('$payment_status','$payment_amount','$payment_currency','$txn_id','$receiver_email','$payer_email','$payment_date')";
                mysqli_query($conn, $sql);
            }
        }

    } else if (strcmp ($res, "INVALID") == 0) {
        // IPN invalid, log for manual investigation
        echo "The response from IPN was: <b>" .$res ."</b>";
    }

?>