<?php
//sesion
define("nokaker","1");

//Principal
include("sources/config.php");
include("sources/functions.php");

//Smarty Nucleo
include("smarty/Smarty.class.php");
$smarty = new Smarty;

//Idioma y sesiones
ob_start();
if(!isset($_SESSION)) session_start();
setlocale(LC_ALL, "es_ES", 'Spanish_Spain', 'Spanish');

//hay tema?
$temita = "";
$titulo = $page_name; //set titulo :v

//Sistema de paginas
cargarSources();

function cargarSources()
{
  //Globales si los hay c:
  global $page_name, $smarty, $temita, $titulo;

  if (empty($_REQUEST['action']))
  {
    if(!$_SESSION['logeado'])
    {
    	require_once('sources/home.php');
      	//Incluimos Header Siempre
		include("header.php");
		$smarty->display((empty($temita)) ? "header.tpl" : $temita."/header.tpl");  
    	return $smarty->display((empty($temita)) ? "home.tpl" : $temita."/home.tpl");
    }
  }
  $actionArray = array(
    //Sistema de Logeo
    'login' => array('login.php', 'logn', false),
    'login2' => array('login.php', 'logn2', false),
    'rpass' => array('login.php', 'relog', false),
    'rpass2' => array('login.php', 'relog2', false),
    'registro' => array('register.php', 'reg', true),
    'registro2' => array('register.php', 'reg2', true),
    'salir' => array('salir.php', 'main', false),
    'salir2' => array('salir.php', 'main2', false),
    'admin' => array('admin.php','main', true),
    'legal' => array('legal.php','main', false),   
    'inmobiliario' => array('inmobiliario.php','main', false),    

    //----------------Paginas Vergas
    //----Admin    
    'categorias' => array('categorias.php','main', true),
    'config' => array('configs.php','main', true),
    'publicidad' => array('publicidad.php','main', true),
    'publicidad2' => array('publicidad.php','guardar', true),
    'users' => array('register.php','main', true),
    //----Post
    'articulo' => array('articulo.php','main', false),
    'post' => array('post.php','main', true),
    'post2' => array('post.php','publicar', true),
    'pre-post' => array('post.php','previsualizar', true),
    //----Otros
    'pagina' => array('pagina.php','verPagina', false),
    'duckapps' => array('duckapps.php','main', false),
    '404' => array('404.php','main', false),
    );

 if($actionArray[$_REQUEST['action']][2] && !$_SESSION['logeado']){
      	require_once('sources/admin.php');
      	//Incluimos Header Siempre
		include("header.php");
		$smarty->display((empty($temita)) ? "header.tpl" : $temita."/header.tpl"); 
      	return $smarty->display("admin.tpl");
  } 
  if (!isset($_REQUEST['action']) || !isset($actionArray[$_REQUEST['action']]))
  {
      	require_once('sources/home.php');
      	//Incluimos Header Siempre
		include("header.php");
		$smarty->display((empty($temita)) ? "header.tpl" : $temita."/header.tpl"); 
      	return $smarty->display((empty($temita)) ? "home.tpl" : $temita."/home.tpl");
  }

	//Obtenemos el config de la pagina
	require_once('sources/' . $actionArray[$_REQUEST['action']][0]);

	//Incluimos Header Siempre
	include("header.php");
	$smarty->display((empty($temita)) ? "header.tpl" : $temita."/header.tpl"); 

	//Mostramos la pagina
	$fileplant = file_exists($temita."/".$_GET['action'].".tpl") ? $temita."/".$_GET['action'].".tpl" : $_GET['action'].".tpl";
	return $smarty->display($fileplant);
}

//Incluimos el footer :3
include("footer.php");
$smarty->display((empty($temita)) ? "footer.tpl" : $temita."/footer.tpl");  
//mysql_close($con);