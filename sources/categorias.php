<?php
if (!defined('nokaker')) {
    die('No se aceptan kakers.');
}
class CategoriasAdmin
{

    public function obCatMain(){
        global $conn;
        $array = array();
        $sql = "SELECT * FROM categorias ORDER BY cOrden ASC";
        $result = mysqli_query($conn, $sql);
        while($result && $cat = mysqli_fetch_assoc($result)) 
        {

            $li = '<li id="image_li_'.$cat['idCat'].'" class="ui-sortable-handle"><a href="javascript:void(0);" style="float:none;" class="image_link button primary">'.$cat['nombre'].'</a> <a href="/index.php?action=admin&p=categorias&b='.$cat['idCat'].'"  class="button warning">Borrar</a></li>     
                        ';
            array_push($array, $li);
        }
        return $array;
    }
    public function borrarCat($id)
    {
        global $conn;
        
        $sql = "DELETE FROM categorias WHERE idCat = $id";
        if (mysqli_query($conn, $sql))
            return '<h2>Borrado con exito!</h2><h5><a href="/index.php?action=admin&p=categorias"><i class="fi-arrow-left"></i> Regresar</a></h5>';        
        else
            return '<h2>Ha ocurrido un error!</h2><h5><a href="/index.php?action=admin&p=categorias"><i class="fi-arrow-left"></i> Regresar</a></h5>';
    }

    public function agregar($nombre){

        global $conn;

        ///agarramos datos
        $nombre = addslashes(htmlentities($nombre));
        
        $sql = "INSERT INTO categorias (nombre) VALUES('$nombre')";
        if (mysqli_query($conn, $sql))
            return '<h2>Categoria creada con exito!</h2><h5><a href="/index.php?action=admin&p=categorias"><i class="fi-arrow-left"></i> Regresar</a></h5>';
        else
            return '<h2>Ha ocurrido un error!</h2><h5><a href="/index.php?action=admin&p=categorias"><i class="fi-arrow-left"></i> Regresar</a></h5>';
    }
}

?>