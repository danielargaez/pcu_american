<?php
  global $YouTubeLive, $conn;
if (!defined('nokaker')) {
    die('No se aceptan kakers.');
}
  
  $idPost = $_GET['i'];
  $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

  $sql = "SELECT * FROM posts 
    INNER JOIN categorias ON categorias.idCat = posts.idCat
    WHERE idPost = $idPost LIMIT 1";
  $result = mysqli_query($conn, $sql);

  //visitas
  $sqls = "UPDATE posts SET visitas = visitas+1 WHERE idPost = $idPost";
  mysqli_query($conn, $sqls);
  //echo $sql;
  if(mysqli_num_rows($result) == 0){
      die('<script>location="/no-encontrado";</script>');
  }

  //Mandamos el array
  $post = mysqli_fetch_assoc($result);
  $smarty->assign("post", $post);

  //Fecha
  $fechita = strtotime($post['fecha']);
  $fechitaview = strftime("%B %d, %Y", $fechita);
  $smarty->assign("fecha",$fechitaview);

  //Cambiamos el titulo de la pagina
  $titulo = $post['titulo'];
  $contenidoC = shorter(trim(strip_tags(html_entity_decode($post['contenido']))), 400);
  //Decimos que estamos en un post
  $smarty->assign("enPost",true);

  //SEO
  $smarty->assign("seoUrl",$actual_link);
  $smarty->assign("seoTitulo",$titulo);
  $smarty->assign("seoDescripcion",$post['nombre'].'.- '.html_entity_decode($contenidoC));
  $smarty->assign("seoImagen",$post['thumbail']);
  //$smarty->assign("seoKeywords",make_keywords($contenidoC));

//agarramos las imgs
             // Image extensions
              $image_extensions = array("png","jpg","jpeg","gif");

              // Target directory
              $galeria ="";
              $dir = 'ajax/uploads/'.$idPost;
              if (is_dir($dir)){
             
               if ($dh = opendir($dir)){
                $count = 1;

                // Read files
                while (($file = readdir($dh)) !== false){

                 if($file != '' && $file != '.' && $file != '..'){
             
                  // Image path
                  $image_path = "ajax/uploads/".$idPost."/".$file;
                  $image_ext = pathinfo($image_path, PATHINFO_EXTENSION);

                  // Check its not folder and it is image file
                  if(!is_dir($image_path) && in_array($image_ext,$image_extensions)){
        
                    $galeria_s .='<div class="mySlides">
                      <div class="numbertext">'.$count.'</div>
                        <img src="'.$image_path.'" style="width:100%; height: 500px;">
                    </div>';
                     $galeria.=' <div class="column">
                        <img class="demo cursor" src="'.$image_path.'" style="width:100%; height:100px;" onclick="currentSlide('.$count.')">
                      </div>';

                   // Break
                   if( $count%4 == 0){

                   }
                   $count++;
                  }
                 }
             
                }
                closedir($dh);
               }
              }
             $smarty->assign("galeria",$galeria);
             $smarty->assign("galeria_s",$galeria_s);


  ////BBCODE
 $contenido = html_entity_decode($post['contenido']);
 $smarty->assign("contenido", $contenido);

function shorter($text, $chars_limit)
{
    // Check if length is larger than the character limit
  if (strlen($text) > $chars_limit)
  {
        // If so, cut the string at the character limit
    $new_text = substr($text, 0, $chars_limit);
        // Trim off white space
    $new_text = trim($new_text);
        // Add at end of text ...
    return $new_text . " [...]";
  }
    // If not just return the text as is
  else
  {
    return $text;
  }
}

function relacionados($titulo)
{
  global $conn;
  $idPost = $_GET['i'];
  $rel = "";
  $sql = "SELECT idPost, titulo, MATCH(titulo, contenido) AGAINST('$titulo') AS score 
  FROM posts 
  WHERE MATCH(titulo, contenido) AGAINST('$titulo') AND idPost != $idPost
  ORDER BY score DESC LIMIT 5";
  $result = mysqli_query($conn, $sql);
  //echo $sql;
  if (mysqli_num_rows($result) > 0) {
      $rel = '<ul>';
      while($post = mysqli_fetch_assoc($result))
      {
        $rel .= '<li>
          <a href="/index.php?action=articulo&i='.$post['idPost'].'">'.$post['titulo'].'</a>
                  </li>';
      }
      $rel .= '</ul>';
    }
    else{
        $rel .= '<ul>';
        $rel .= ' <li>Aun no hay relacionados.</li>';
        $rel .= '</ul>';
    }
  return $rel;
}
$smarty->assign("relacionados", relacionados($titulo));


function make_keywords( $pContent, $config = null )
    {
        # let's make an in_array case-insensitive
        if(!function_exists('in_arrayi')):
            function in_arrayi( $needle, $haystack ){
                return in_array( strtolower( $needle ), array_map( 'strtolower', $haystack ) );
            }
        endif;
 
        # initializing some default vars values
        $min_word_length = 4;
        # limit the number of output keywords
        $limit_keywords = 1000;
        # a set of disallowed words goes here
        $arr_avoid = array();
        # check if config param is set
        if(isset($config)):
            # looking for a minimum word length
            if(isset( $config['min_allowed_word_length'] )) 
                $min_word_length = $config['min_allowed_word_length'];
            # checking for a new limit
            if(isset( $config['limit_keywords_to'] )) 
                $limit_keywords = $config['limit_keywords_to'];
            # check if there's words to avoid
            if(isset( $config['avoid_words'] ) && ( is_array( $config['avoid_words'] ))) 
                $arr_avoid = $config['avoid_words'];
            # maybe the avoid_words is pointing to a file! lets check!
            if(isset( $config['avoid_words'] ) && ( is_string( $config['avoid_words']) )):
                if(file_exists( $config['avoid_words'] )){
                    # let's open the file in read mode
                    $file_in = fopen( $config['avoid_words'],"r" );
                    # let's get the filesize for the fread function
                    $size = filesize( $config['avoid_words'] ) + 1;
                    # getting the file content
                    $file_content = fread( $file_in, $size );
                    # avoid any tags
                    $file_content = strip_tags( $file_content );
                    # turn content into array
                    # remember that the words within the file must be separated by commas
                    $file_content = str_replace( " ", "", $file_content );
                    $arr_words = explode(",", $file_content);
                    if( is_array( $arr_words) )
                        $arr_avoid = $arr_words;
                }
            endif;
        endif;
 
        # sometimes need use utf8 decode to fix some character issues
      /*  if( !isset( $config['use_utf8_decode'] ) ||
            (isset( $config['use_utf8_decode']) &&
                $config['use_utf8_decode'] == TRUE ) )
            $pContent = utf8_decode($pContent);*/
 
        //$pContent = html_entity_decode($pContent);
        $pContent = strip_tags($pContent);
        # preparando conteudo para conversao
        $content = str_replace( " ", "@", $pContent );
        # remove sinais de pontuacao
        $strip_arr = array( "" ,"." ,":", "\"", "'", "�","�","(",")", "!","?",";","�");
        //$content = str_replace( $strip_arr, "", $content );
        # separa palavras, e coloca as num array
        $arr_keywords = explode( "@", $content );
        # verifica tamanho da palavra para evita artigos, pronomes, etc
        # e tambem verifica se ha palavras nao desejadas
        $key = array();
        foreach ($arr_keywords as $value) :
            if( strlen( $value ) > $min_word_length ):
                # verifica por palavras nao desejadas
                if ( in_arrayi( $value, $arr_avoid ) ) continue;
                # se potencial keyword, entao, inclui ao array
                $keys[] = $value;
            endif;
        endforeach;
        # count the number of occurrences
        $tmp = array_count_values( $keys );
        # check if needs to order the array
        if(!isset($config['order_by_occurence'])
            || (isset($config['order_by_occurence'])
                && $config['order_by_occurence'] == TRUE ) )
            arsort( $tmp );
        # reduces the number of elements into the array
        $ordem = array_slice( $tmp, 0, $limit_keywords );
        $arr_keywords = array();
        # gets only the keys
        foreach($ordem as $key => $value ) 
            $arr_keywords[] = strtolower(trim( $key ));
        # implode elements with commas
        $keywords = implode( ', ', $arr_keywords );
        $keywords = str_replace( $strip_arr, "", html_entity_decode($keywords));
        return $keywords;
    }
?>