<?php
global $conn;
if (!defined('nokaker')) {
    die('No se aceptan kakers.');
}

//confgis
$titulo = "Administracion";
$p = $_GET['p'];
$smarty->assign("p",$p);

switch ($p) {
	case 'login':
		$titulo = "Iniciar Sesion";
		if($_POST)
		{			
			  $sd = $_SESSION['uID'];
			  $correo = htmlentities($_POST['correo']);
			  $pass = htmlentities($_POST['pass']);

			  $_SESSION['login_ses'] = session_id(); //obtenemos la sesionid
			  $sql = "SELECT correo, acc_pass, superadmin, acc_name FROM masters WHERE correo = '$correo' LIMIT 1";
			  $result = mysqli_query($conn, $sql);
			  //echo $sql;
			  if (mysqli_num_rows($result) > 0) 
			  {
			      // output data of each row
			      while($row = mysqli_fetch_assoc($result)) 
			      {
			      	  //
			        if($row['acc_pass'] == sha1($pass))
			        {
			            $_SESSION['correoC'] = $row['correo'];
			            $_SESSION['nombreAActual'] = $row['acc_name'];
			            $_SESSION['superadmin'] = $row['superadmin'];
			            $_SESSION['envivoID'] = 0;         
			            //obtenemos
			            $_SESSION['logeado'] = true;
			            $_SESSION['registradopcu'] = true;
			            echo'<script>location="/";</script>';
			        }
			        else
			        {
			         $texto_aviso = '
			              Tu contraseña no es la correcta (Se te redireccionara en 3 segundos)
			              <script>
			              setTimeout("goBack()",3000);                
			              function goBack() {
			                location="/index.php?action=admin&p=login&e='.$correo.'";
			              }</script>';
			        }
			      }
			  } else {
			            $texto_aviso = '<script>location="/index.php?action=admin&p=login&e='.$correo.'";</script>';
			  }
			  $smarty->assign("pagina","pagina_aviso.tpl");
		}
		else
			$smarty->assign("pagina","admin/login.tpl");
		break;
	case 'rpass':
		if($_POST)
		{

		  $correo = htmlentities($_POST['correo']);
		  $pass = htmlentities($_POST['sword']);
		  $npass = htmlentities($_POST['npass']);
		  $rnpass = htmlentities($_POST['rnpass']);

		  $sql = "SELECT correo, acc_dbid, acc_pass, superadmin, acc_name, secret_word FROM masters WHERE correo = '$correo' LIMIT 1";
		  $result = mysqli_query($conn, $sql);
		  //echo $sql;
		  if (mysqli_num_rows($result) > 0) 
		  {
		      // output data of each row
		      while($row = mysqli_fetch_assoc($result)) 
		      {
		          //
		        if($row['secret_word'] == $pass)
		        {
		            if($npass == $rnpass)
		            {
		              $uid = $row['acc_dbid'];
		              $sql = "UPDATE masters SET acc_pass = sha1('$npass') WHERE acc_dbid = $uid";
		              if(mysqli_query($conn, $sql))
		                echo'<script>location="/admin";</script>';
		              else
		                echo'<script>location="/index.php?action=admin&p=rpass";</script>';                
		            }
		            else {echo'
		              Las contraseñas son distintas (Se te redireccionara en 3 segundos)
		              <script>
		              setTimeout("goBack()",3000);                
		              function goBack() {
		                location="/index.php?action=admin&p=rpass&e='.$correo.'";
		              }</script>';}
		        }
		        else
		        {
		          echo'
		              La palabra secreta no es la correcta (Se te redireccionara en 3 segundos)
		              <script>
		              setTimeout("goBack()",3000);                
		              function goBack() {
		                location="/index.php?action=admin&p=rpass&e='.$correo.'";
		              }</script>';
		        }
		      }
		  } else {
		            echo'<script>location="/index.php?action=admin&p=rpass&e='.$correo.'";</script>';
		  }
		}
		else
			$smarty->assign("pagina","admin/rpass.tpl");	
		break;
	case 'publicidad':
		if($_POST) 
      guardarNews();
		$smarty->assign("pagina","admin/publicidad.tpl");
		break;
  case 'post':
        //para borrar
        if(isset($_GET['b']))
            die(borrarPost($_GET['b']));

        if(isset($_GET['pub']))
            die(publicar());

        $titulo = ''.(($_GET['e']) ? 'Editar Articulo' : 'Crear nuevo articulo').'';

        if(!empty($_GET['e']))
        {
              $idPost = $_GET['e'];
              $smarty->assign("idPost",$idPost);

              $sql = "SELECT * FROM posts WHERE idPost = $idPost";
              $result = mysqli_query($conn, $sql);
              $post = mysqli_fetch_assoc($result);

              ////BBCODE
             $tituloPost = html_entity_decode($post['titulo'], ENT_QUOTES, "UTF-8");
             $smarty->assign("tituloPost",$tituloPost);

             $contenido = $post['contenido'];
             $smarty->assign("contenido",$contenido);

             $etiquetas = htmlentities($post['etiquetas']);
             $smarty->assign("etiquetas",$etiquetas);

             $categoria = htmlentities($post['idCat']);
             $smarty->assign("categoria",$categoria);

             $thumb = htmlentities($post['thumbail']);
             $smarty->assign("thumb", $thumb);

             $comentarios = htmlentities($post['comentarios']);
             $smarty->assign("comentarios",$comentarios);

             //agarramos las imgs
             // Image extensions
              $image_extensions = array("png","jpg","jpeg","gif");

              // Target directory
              $galeria ="";
              $dir = 'ajax/uploads/'.$idPost;
              if (is_dir($dir)){
             
               if ($dh = opendir($dir)){
                $count = 1;

                // Read files
                while (($file = readdir($dh)) !== false){

                 if($file != '' && $file != '.' && $file != '..'){
             
                  // Image path
                  $image_path = "ajax/uploads/".$idPost."/".$file;
                  $image_ext = pathinfo($image_path, PATHINFO_EXTENSION);

                  // Check its not folder and it is image file
                  if(!is_dir($image_path) && in_array($image_ext,$image_extensions)){
        
                     $galeria.='<img style="width:100px;" src="'.$image_path.'" alt="" title=""/>';

                   // Break
                   if( $count%4 == 0){

                   }
                   $count++;
                  }
                 }
             
                }
                closedir($dh);
               }
              }
             $smarty->assign("galeria",$galeria);

             /*$precio = htmlentities($post['precio']);
             $smarty->assign("precio",$precio);*/
             $smarty->assign("e",$_GET['e']);
        }

        $sql = "SELECT idCat, nombre FROM categorias";
        $result = mysqli_query($conn, $sql);
        $catos = array();
        while($cat = mysqli_fetch_assoc($result))
        {
            $li = '<option value="'.$cat['idCat'].'"'; 
            if($cat['idCat'] ==  $categoria) {$li.= 'selected';} 
            $li.= '>'.$cat['nombre'].'</option>';
            array_push($catos, $li);
        }
        $smarty->assign("cats",$catos);

        $smarty->assign("e",$_GET['e']);
        $smarty->assign("pagina","admin/post.tpl");
        break;
  case 'paginas':
        //objeto
        require_once("pagina.class.php");
        $paginasO = new PaginasAdmin;
        //para borrar
        if(isset($_GET['b']))
            die($paginasO->borrarPagina($_GET['b']));

        if(isset($_GET['pub']))
            die($paginasO->crear());

        $titulo = ''.(($_GET['e']) ? 'Editar Pagina' : 'Crear Pagina').'';

        if(!empty($_GET['e']))
        {
            $idPag = $_GET['e'];
            $smarty->assign("e",$_GET['e']);

            $smarty->assign("idPag",$idPag);
            $sql = "SELECT * FROM paginas WHERE idPag = $idPag";
            $result = mysqli_query($conn, $sql);
            $post = mysqli_fetch_assoc($result);

              ////BBCODE
            $titulo = htmlentities($post['titulo']);
            $smarty->assign("titulo",$titulo);

            $menu = htmlentities($post['menu']);
            $smarty->assign("menu",$menu);

            $comentarios = htmlentities($post['comentarios']);
            $smarty->assign("comentarios",$comentarios);

            $contenido = $post['contenido'];
            $smarty->assign("contenido",$contenido);
            $smarty->assign("pagina","admin/paginas_nuevo.tpl");   
        }
        else if(!empty($_GET['c']))
        {            
            $smarty->assign("pagina","admin/paginas_nuevo.tpl");   
        }
        else
        {
            $smarty->assign("paginasMain",$paginasO->obPaginasMain());
            $smarty->assign("pagina","admin/paginas.tpl");   
        }     
        break;
  case 'categorias':    
    require_once("categorias.php");
    $catsAdmin = new CategoriasAdmin;
    $nseccion = true;
    if(!empty($_GET['b']))
       $smarty->assign("seccion", $catsAdmin->borrarCat($_GET['b']));
    else if(!empty($_GET['s']))
       $smarty->assign("seccion", $catsAdmin->agregar($_POST['nombre']));
    else 
      $nseccion = false;

    $smarty->assign("catadmin",$catsAdmin->obCatMain());
    $smarty->assign("enseccion", $nseccion);
    $smarty->assign("pagina","admin/categorias.tpl");
    break;
  case 'users':
    require_once("register.php");
    $usersAdmin = new UsersAdmin;
    $nseccion = true;

    if(!empty($_GET['b']))
      $smarty->assign("seccion", $usersAdmin->borrarAdmin($_GET['b']));
    else if(!empty($_GET['c']) || !empty($_GET['ed']))
      $smarty->assign("seccion", $usersAdmin->reg());
    else if(!empty($_GET['cc']))
      $smarty->assign("seccion", $usersAdmin->reg2());
    else
      $nseccion = false;

    $smarty->assign("usersadmin", $usersAdmin->obUsersMain());
    $smarty->assign("enseccion", $nseccion);
    $smarty->assign("pagina","admin/users.tpl");
    break;  
	default:
		$smarty->assign("pagina","admin/home.tpl");
		break;
}


function borrarPost($id)
{
    global $conn;
    
    $sql = "DELETE FROM posts WHERE idPost = $id";
    if (mysqli_query($conn, $sql)) {
        echo'<h2>Borrado con exito!</h2><h5><a href="/"><i class="fi-arrow-right"></i> Ir al Inicio</a></h5>';
    }
    else{   echo'<h2>Ha ocurrido un error!</h2><h5><a href="/"><i class="fi-arrow-right"></i> Ir al Inicio</a></h5>';}
}

function publicar(){

    global $conn;

    ///agarramos datos
    $titulo = htmlentities(addslashes($_POST['titulo']), ENT_QUOTES,'UTF-8');
    $contenido = htmlentities(addslashes($_POST['contenido']));
    $etiquetas = htmlentities(addslashes($_POST['etiquetas']));
    $categoria = htmlentities(addslashes($_POST['categoria']));
    $creador = htmlentities(addslashes($_POST['creador']));
    $thumb = htmlentities(addslashes($_POST['thumbnail']));
    $comentarios = htmlentities(addslashes($_POST['comentarios']));
    //$precio = htmlentities(addslashes($_POST['precio']));
    $eidPost = htmlentities($_POST['edit']);
   // echo $titulo;
   // echo $contenido;
  //  echo $etiquetas;
  //  echo $categoria;
    $hoy = date("Y-m-d H:i:s"); 
    
    if(isset($comentarios) && $comentarios == '1')
      $comentarios = 1;
    else 
      $comentarios = 0;
    
    if(empty($eidPost))
        $sql = "INSERT INTO posts (titulo, contenido, etiquetas, idCat, fecha, thumbail, creador) VALUES('$titulo', '$contenido', '$etiquetas', $categoria, '$hoy','$thumb', '$creador')";
    else
        $sql = "UPDATE posts SET titulo = '$titulo', contenido = '$contenido', idCat = $categoria, etiquetas = '$etiquetas', thumbail = '$thumb', comentarios = $comentarios WHERE idPost = $eidPost";
    if (mysqli_query($conn, $sql)) {
   // echo $output.'<hr>';

        echo'<h2>Publicado con exito!</h2><h5><a href="/inmobiliario/"><i class="fi-arrow-right"></i> Ir al Inicio</a></h5>';
    }
    else{   echo'<h2>Ha ocurrido un error!</h2><h5><a href="/admin"><i class="fi-arrow-right"></i> Ir al Inicio</a></h5>';}
}

?>