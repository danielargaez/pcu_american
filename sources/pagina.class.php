<?php
if (!defined('nokaker')) {
    die('No se aceptan kakers.');
}
class PaginasAdmin
{
    public function obPaginasMain()
    {
      global $conn;
      $array = array();
      $sql = "SELECT idPag, titulo FROM paginas";
      $result = mysqli_query($conn, $sql);

      if (mysqli_num_rows($result) > 0) {
          while($post = mysqli_fetch_assoc($result))
          {
              $li =' <li>
                <a href="/'.$post['idPag'].'/'.$post['titulo'].'.html">'.$post['titulo'].'</a> 
                <a href="/index.php?action=cpagina&e='.$post['idPag'].'" style="float:right;"><span class="fi-pencil"></span> Editar</a>
                </li>';
              array_push($array, $li);
          }
        }
        else{
            $li = '<li>Aun no hay paginas creadas.</li>';
            array_push($array, $li);
        }
        return $array;
    }

    public function borrarPagina($id)
    {
        global $conn;
        
        $sql = "DELETE FROM paginas WHERE idPag = $id";
        if (mysqli_query($conn, $sql)) {
            echo'<h2>Borrado con exito!</h2><h5><a href="/"><i class="fi-arrow-right"></i> Ir al Inicio</a></h5>';
        }
        else{   echo'<h2>Ha ocurrido un error!</h2><h5><a href="/"><i class="fi-arrow-right"></i> Ir al Inicio</a></h5>';}
    }

    public function crear(){

        global $conn;

        ///agarramos datos
        $titulo = addslashes(htmlentities($_POST['titulo']));
        $contenido = addslashes(htmlentities($_POST['contenido']));
        $comentarios = addslashes(htmlentities($_POST['comentarios']));
        $menu = addslashes(htmlentities($_POST['menu']));
        $eidPag = htmlentities($_POST['edit']);

        if(isset($menu) && $menu == '1')
          $menu = 1;
        else
          $menu = 0;

        if(isset($comentarios) && $comentarios == '1')
          $comentarios = 1;
        else 
          $comentarios = 0;

        //echo $menu;
        //echo $comentarios;
        
        if(empty($eidPag))
            $sql = "INSERT INTO paginas (titulo, contenido, menu, comentarios) VALUES('$titulo', '$contenido', $menu, $comentarios)";
        else
            $sql = "UPDATE paginas SET titulo = '$titulo', contenido = '$contenido', menu = $menu, comentarios = $comentarios WHERE idPag = $eidPag";
        if (mysqli_query($conn, $sql)) {
       // echo $output.'<hr>';

            echo'<h2>Proceso terminado con exito!</h2><h5><a href="/"><i class="fi-arrow-right"></i> Ir al Inicio</a></h5>';
        }
        else{   echo'<h2>Ha ocurrido un error!</h2><h5><a href="/"><i class="fi-arrow-right"></i> Ir al Inicio</a></h5>';}
    }


    public function verPagina($idPag){

      global $conn, $smarty;
      $smarty->assign("idPag",$idPag);

      $sql = "SELECT * FROM paginas WHERE idPag = $idPag";
      $result = mysqli_query($conn, $sql);
      //if (mysqli_num_rows($result) != 0) 
      $post = mysqli_fetch_assoc($result);

      $sqls = "UPDATE paginas SET visitas = visitas+1 WHERE idPag = $idPag";
      mysqli_query($conn, $sqls);
      //echo $sql;

      $sqls = "SELECT * FROM configs";
      $cf = mysqli_query($conn, $sqls);
      $config = mysqli_fetch_assoc($cf);


      ////BBCODE
     $contenido = html_entity_decode($post['contenido']);
     $smarty->assign("PagContenido",$contenido);   
     $smarty->assign("PagTitulo",$post['titulo']);   
     $smarty->assign("PagComentarios",$post['comentarios']); 


      $fechita = strtotime($post['fecha']);
      $fechitaview = date("m F, Y", $fechita);
      $smarty->assign("fechitaview",$fechitaview);  

    }

//fin class
}

?>