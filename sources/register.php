<?php
if (!defined('nokaker')) {
    die('No se aceptan kakers.');
}
class UsersAdmin
{
    public function borrarAdmin($id)
    {
        global $conn;        
        $sql = "DELETE FROM masters WHERE acc_dbid = $id";
        if (mysqli_query($conn, $sql)) 
          return '<h2>Borrado con exito!</h2><h5><a href="/admin"><i class="fi-arrow-right"></i> Ir al Inicio</a></h5>';
        else
          return '<h2>Ha ocurrido un error!</h2><h5><a href="/admin"><i class="fi-arrow-right"></i> Ir al Inicio</a></h5>';
    }

    public function obUsersMain()
    {
      global $conn;
      $array = array();
      $sql = "SELECT acc_dbid, acc_name, correo FROM masters WHERE acc_name != 'Admin'";
      $result = mysqli_query($conn, $sql);

      if (mysqli_num_rows($result) > 0) {
          while($post = mysqli_fetch_assoc($result))
          {
            $li = '<div class="reveal"  style="display:none;" id="animatedModal'.$post['acc_dbid'].'" data-reveal data-close-on-click="true" data-animation-in="spin-in" data-animation-out="spin-out">
    			  <h1>¿Seguro que desea borrar el administrador?</h1>
    			  <center><a href="/index.php?action=admin&p=users&b='.$post['acc_dbid'].'" class="button alert" >Borrar</a>
    			  <a data-close  class="button" >No</a>
    			  </center>
    			  <button class="close-button" data-close aria-label="Close reveal" type="button">
    			    <span aria-hidden="true">&times;</span>
    			  </button>
    			</div>
    			<tr>
    	      <td style="text-align: center;">'.$post['acc_dbid'].'</td>
    	      <td style="text-align: center;">'.$post['acc_name'].'</td>
    	      <td style="text-align: center;">'.$post['correo'].'</td>
    	      <td style="text-align: center;">No</td>
    	      <td style="text-align: center;"><a class="button warning" href="/index.php?action=admin&p=users&ed='.$post['acc_dbid'].'"><span class="fi-widget"></span> Editar</a> <a data-toggle="animatedModal'.$post['acc_dbid'].'" class="button alert"><span class="fi-x"></span> Borrar</a></td>
    	      </tr>';
            array_push($array, $li);
          }
        }
        else{
            $li = '<tr>Aun no hay mas Administradores.</tr>';
            array_push($array, $li);
        }
      return $array;
    }
    public function reg()
    {
      //enviamos al search si existe un cat
      global $conn;
      $html = "";
      if($_SESSION['superadmin'] == 0)
      {
      	return ("No tienes permiso para esta pagina.");
      }
      else {
      //((!empty($_GET['ed'])) ? '' : '')
      $ed = $_GET['ed'];

      if(isset($_GET['b']))
          return (borrarAdm($_GET['b']));


      if(!empty($ed)){
        $idAdm = $_GET['ed'];
        $sql = "SELECT * FROM masters
        WHERE acc_dbid = $idAdm LIMIT 1";
        $result = mysqli_query($conn, $sql);
        $adm = mysqli_fetch_assoc($result);

      }
      $html .= '<div class="container has-text-centered">

              <div class="notification">
                  <h2>
                    '.((!empty($ed)) ? 'Editar Administrador' : 'Agregar Administrador').'
                  </h2>
                  <p class="subtitle">
                  Rellene los datos a continuacion para 
                   '.((!empty($ed)) ? 'editar un Administrador.' : 'agregar un nuevo administrador.').'
                    
                  </p>
        <div class="columns">
          <div class="column">      
                <form action="/index.php?action=admin&p=users&cc=1'.((!empty($ed)) ? '&uid='.$_GET['ed'] : '').'" method="post">
      			<label><h6><i class="fi-at-sign"></i> Correo Electronico:</h6>			  
                        <input  type="email" value="'.((!empty($ed)) ? $adm['correo'] : '').'" name="email"  placeholder="Correo electronico" required>
      			</label>
      			<label><h6><i class="fi-torso"></i> Nombre:</h6>			  
                        <input type="text" value="'.((!empty($ed)) ? $adm['acc_name'] : '').'" name="nombre"  placeholder="Nombre" required>
      			</label>'; 
      			if(empty($ed)){$html .= '
      			<label><h6><i class="fi-bookmark"></i> Palabra Secreta</h6><input type="text" name="sword" placeholder="Palabra Secreta" required></label>
      			<label><h6><i class="fi-lock"></i> Contraseña:</h6>			  
                        <input  type="password" name="pass" placeholder="Contraseña" required>
      			</label>
      			
      			<label><h6><i class="fi-lock"></i> Ingrese nuevamente su contraseña:</h6>			  
                        <input  type="password" name="repass" placeholder="Contraseña" required>
      			</label>';}
                  $html .= '</div>
                  <div class="field is-grouped">
                    <div class="control">
                      <button type="submit" class="button success">'.((!empty($ed)) ? 'Guardar Datos': 'Registrar Datos').'</button>  <a class="button is-text" href="/admin">Cancelar</a>
                    </div>
                  </div>
                </form>
          </div>';}
        return $html;
    }
    public function reg2()
    {
      global $conn;

      //obtenemos
      $nombre = htmlentities($_POST['nombre']);
      $password = htmlentities($_POST['pass']);
      $repassword = htmlentities($_POST['repass']);
      $email = htmlentities($_POST['email']);
      $sword = htmlentities($_POST['sword']);
      $ed = $_GET['uid'];
      //$nombre = str_replace(' ', '-', $nombre); // Replaces all spaces with hyphens.
      //$nombre = preg_replace('/[^A-Za-z\-]/', '', $nombre); // Removes special chars.
      $errorC = 0;
      if(!empty($email))
      {
        //Verificamos que no exista otro correo igual.
        $sql = "SELECT correo FROM masters WHERE correo = '$email' LIMIT 1";
        $result = mysqli_query($conn, $sql);
        if (mysqli_num_rows($result) > 0 && empty($ed)) 
        {
            $errorC = 1;
        }
      } 
      else 
        return ('Escriba un correo electronico! <br><input class="input is-large" type="button" onclick="window.history.back(); return false;" class="btn btn-primary" value="Regresar">');
      //Verificamos formulario
      if($repassword != $password) 
        return ('Sus contraseñas son diferentes. <br><input class="input is-large" type="button" onclick="window.history.back(); return false;" class="btn btn-primary" value="Regresar">');

      if($errorC == 0)
      {
        $fech = date("Y-m-d");

        if(empty($ed))
        $sql = "INSERT INTO masters (acc_name, acc_pass, register_date, superadmin, correo, secret_word) VALUES('$nombre', sha1('$password'), '$fech', 0, '$email', '$sword')";
    	else
    		$sql = "UPDATE masters SET acc_name = '$nombre', correo = '$email' WHERE acc_dbid = $ed";

        $html = '<ul class="ca bkt bku abk">
                <li class="oq b acx">
                  <div class="or">';
        if (mysqli_query($conn, $sql)) {
        $html .= '<div class="bky">
                      <h3 style="color: #21425d;margin-left: -17px;"><i class="fa fa-address-card fa-fw" aria-hidden="true"></i>&nbsp; 
                      '.((!empty($ed)) ? 'Guardado Exitoso!' : 'Registro Exitoso!').'</h2>
                    </div>La cuenta ha sido 
                    '.((!empty($ed)) ? 'guardada con exito' : 'creada satisfactoriamente').', ya se puede <strong>logear</strong> normalmente.<br>
                    <small>Se te redireccionara en 5 segundos.</small>
                    <script>
                      function mandar(){
                        location="/admin";
                      }
                      setTimeout("mandar()",3000);
                    </script>
                </div>';
        } else {
            $html .= '<div class="bky">
                      <h3 style="color: #21425d;margin-left: -17px;"><i class="fa fa-address-card fa-fw" aria-hidden="true"></i>&nbsp; Registro Erroneo!</h2>
                    </div>
                    Hubo un error en la base de datos, intenta <a href="/index.php?action=registro"><strong>registrar</strong></a> una vez mas.
                </div>';
        }
        $html.= '</li></ul>';
        return $html;
      }
      else{
        return 'Ese correo electronico ya está en uso. <br><input class="input is-large" type="button" onclick="window.history.back(); return false;" class="btn btn-primary" value="Regresar">';
      }
    }

}

?>