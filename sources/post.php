<?php
if (!defined('nokaker')) {
    die('No se aceptan kakers.');
}


function main()
{
  global $conn;
//para borrar
if(isset($_GET['b']))
    die(borrarPost($_GET['b']));

echo'<h2>'.(($_GET['e']) ? 'Editar Articulo' : 'Crear nuevo articulo').'</h2>';

if(!empty($_GET['e']))
{


  $idPost = $_GET['e'];

  $sql = "SELECT * FROM posts WHERE idPost = $idPost";
  $result = mysqli_query($conn, $sql);
  $post = mysqli_fetch_assoc($result);

  ////BBCODE
 $titulo = html_entity_decode($post['titulo'], ENT_QUOTES, "UTF-8");
 $contenido = $post['contenido'];
 $etiquetas = htmlentities($post['etiquetas']);
 $categoria = htmlentities($post['idCat']);
 $thumb = htmlentities($post['thumbail']);
 $comentarios = htmlentities($post['comentarios']);

 // Image extensions
  $image_extensions = array("png","jpg","jpeg","gif");

  // Target directory
  $dir = 'ajax/uploads/'.$idPost;
  if (is_dir($dir)){
 
   if ($dh = opendir($dir)){
    $count = 1;

    // Read files
    while (($file = readdir($dh)) !== false){

     if($file != '' && $file != '.' && $file != '..'){
 
      // Image path
      $image_path = "images/".$idPost."/".$file;
      $image_ext = pathinfo($image_path, PATHINFO_EXTENSION);

      // Check its not folder and it is image file
      if(!is_dir($image_path) && in_array($image_ext,$image_extensions)){

?>
<a href="<?php echo $image_path; ?>">
        <img src="<?php echo $thumbnail_path; ?>" alt="" title=""/>
       </a>
<?php

       // Break
       if( $count%4 == 0){

       }
       $count++;
      }
     }
 
    }
    closedir($dh);
   }
  }
 //$precio = htmlentities($post['precio']);

}
echo'
<!-- Init WysiBB BBCode editor -->  
<script src="/js/tinymce/tinymce.min.js"></script>
  <script>tinymce.init({
  selector: "textarea",
  height: 450,
  plugins: [
    "autoresize advlist autolink autosave link image lists charmap print preview hr anchor pagebreak",
    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
    "table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern"
  ],

  toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
  toolbar2: "forecolor backcolor | searchreplace | bullist numlist | outdent indent blockquote | link unlink anchor image media code | insertdatetime preview",
  toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | visualchars visualblocks nonbreaking pagebreak restoredraft | undo redo ",
  images_upload_url: \'/ajax/subir-foto.php\',
  images_upload_credentials: true,
  menubar: false,
  autoresize_max_height: 600
});
</script>
<script>

function cerrarPre(){
    $("#editor_previsualizar").fadeOut("fade");
    $("#editor_previsualizar_btn").fadeOut("fade");
    $("#editor_post").fadeIn("fade");
}

</script>

<div id="editor_previsualizar"></div>
<button id="editor_previsualizar_btn" style="display:none;" onclick="cerrarPre(); return false;" class="primary button">Cerrar Previsualizacion</button>
<div id="editor">
<form id="idForm" method="post" action="/index.php?action=post2">
<div id="editor_post">
<label><h6>Titulo:</h6>
  <input type="text" name="titulo" value="'.$titulo.'" placeholder="Titulo del articulo" maxlength="100" required>
</label>
<p class="help-text" id="passwordHelpText">El titulo tiene un maximo de 100 caracteres.</p>

<label><h6>Contenido:</h6>
      <textarea id="edit" name="contenido" style="min-height:300px;" placeholder="Escribir algo...">'.html_entity_decode($contenido).'</textarea>
</label>
<br>

<label><h6>Imagen Representativa o Miniatura:</h6>
  <input type="text" name="thumbnail" value="'.$thumb.'" placeholder="Ingresar enlace de imagen" maxlength="100" required>
</label>
<p class="help-text" id="passwordHelpText">Los thumbnails o miniaturas son versiones de imágenes, usadas para ayudar a su organización y reconocimiento.<br>
Se pueden subir imagenes en la opcion de la derecha llamada \'Subir Archivo\'. </p>
<label><h6>Etiquetas:</h6>
  <input type="text" name="etiquetas" value="'.$etiquetas.'" placeholder="etiqueta, etiqueta2, etiqueta3" maxlength="100" required>
</label>
<p class="help-text">Se necesitan minimo <b>3 etiquetas</b> separadas por comas. <br><b>Ejemplo:</b> musica, mexico, accion</p>
<label><h6>Categoria:</h6>
  <select name="categoria" required>';
  $sql = "SELECT * FROM categorias";
  $result = mysqli_query($conn, $sql);
  while($cat = mysqli_fetch_assoc($result)) 
  {
    echo'<option value="'.$cat['idCat'].'"'; if($cat['idCat'] ==  $categoria) {echo'selected';} echo'>'.$cat['nombre'].'</option>';
  }
  echo'
  </select>
</label><br>
<label><h6>Opciones</h6></label>
<label><input type="checkbox" name="comentarios" value="1" '.(($comentarios == 1) ? 'checked' : '').' class="check"> Habilitar caja de comentarios</h6></label>
<br>';

if(!empty($_GET['e']))
    echo'<input type="hidden" name="edit" value="'.$idPost.'">';
  else
    echo'<input type="hidden" name="creador" value="'.$_SESSION['nombreAActual'].'">';

echo'<button type="submit" name="borrador"  value="1" class="success button" disabled>Guardar en Borradores</button>
<input type="submit" class="primary button" value="'.(($_GET['e']) ? 'Guardar' : 'Publicar').'">
</div>
</form></div>
';
}

function borrarPost($id)
{
    global $conn;
    
    $sql = "DELETE FROM posts WHERE idPost = $id";
    if (mysqli_query($conn, $sql)) {
        echo'<h2>Borrado con exito!</h2><h5><a href="/"><i class="fi-arrow-right"></i> Ir al Inicio</a></h5>';
    }
    else{   echo'<h2>Ha ocurrido un error!</h2><h5><a href="/"><i class="fi-arrow-right"></i> Ir al Inicio</a></h5>';}
}

function publicar(){

    global $conn;

    ///agarramos datos
    $titulo = htmlentities(addslashes($_POST['titulo']), ENT_QUOTES,'UTF-8');
    $contenido = htmlentities(addslashes($_POST['contenido']));
    $etiquetas = htmlentities(addslashes($_POST['etiquetas']));
    $categoria = htmlentities(addslashes($_POST['categoria']));
    $creador = htmlentities(addslashes($_POST['creador']));
    $thumb = htmlentities(addslashes($_POST['thumbnail']));
    $comentarios = htmlentities(addslashes($_POST['comentarios']));
    //$precio = htmlentities(addslashes($_POST['precio']));
    $eidPost = htmlentities($_POST['edit']);
   // echo $titulo;
   // echo $contenido;
  //  echo $etiquetas;
  //  echo $categoria;
    $hoy = date("Y-m-d H:i:s"); 
    
    if(isset($comentarios) && $comentarios == '1')
      $comentarios = 1;
    else 
      $comentarios = 0;
    
    if(empty($eidPost))
     {   $sql = "INSERT INTO posts (titulo, contenido, etiquetas, idCat, fecha, thumbail, creador) VALUES('$titulo', '$contenido', '$etiquetas', $categoria, '$hoy','$thumb', '$creador')";
    }
    else
        $sql = "UPDATE posts SET titulo = '$titulo', contenido = '$contenido', idCat = $categoria, etiquetas = '$etiquetas', thumbail = '$thumb', comentarios = $comentarios WHERE idPost = $eidPost";
    if (mysqli_query($conn, $sql)) {
   // echo $output.'<hr>';

        echo'<h2>Publicado con exito!</h2><h5><a href="/"><i class="fi-arrow-right"></i> Ir al Inicio</a></h5>';
    }
    else{   echo'<h2>Ha ocurrido un error!</h2><h5><a href="/"><i class="fi-arrow-right"></i> Ir al Inicio</a></h5>';}
}

?>