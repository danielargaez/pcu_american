<?php

global $conn;
if (!defined('nokaker')) {
	die('No se aceptan kakers.');
}

$titulo = "Portafolio";
$idCat = $_GET['idCat'];
$p = $_GET['p'];

//Categorias
$sql = "SELECT idCat, nombre FROM categorias WHERE tipo = 2 ORDER BY cOrden";
$result = mysqli_query($conn, $sql);
//echo $sql;
if($result)
{
	if(mysqli_num_rows($result) > 0)
	{
		$smarty->assign("hayC",true);
		$smarty->assign("num_cats",mysqli_num_rows($result));
		$categorias = array();
		while($cat = mysqli_fetch_assoc($result))
		{

			$newdata = array(
				'id' => $cat['idCat'],
				'nom' => $cat['nombre'],
				'active' => ($idCat == $cat['idCat'])
			);

			//Cambiar Titulo
			if($newdata['active'])
	    		$titulo = $newdata['nom'].' | '.$titulo;

	    	$categorias[] = $newdata; //agregar
	    }
	    $smarty->assign("catPort",$categorias);
	}
}


//paginacion
if (!isset($p) or !is_numeric($p)) {
	$startrow = 0;
} else {
	$startrow = ($p * 10);
}

//Fotos
$sql = "SELECT * FROM portafolio";
if(!empty($idCat))
	$sql.=" WHERE idCat = ".$idCat;
else
	$smarty->assign("enCat",true);
$sql .= " ORDER BY idPort DESC LIMIT $startrow, 10";
$result = mysqli_query($conn, $sql);
 //echo $sql;
$hayposts = 0;
if($result)
{
	if(mysqli_num_rows($result) > 0)
	{
		$smarty->assign("hay",true);
		$smarty->assign("num_posts",mysqli_num_rows($result));
		$buscFotos = array();
		while($post = mysqli_fetch_assoc($result))
		{
			$newdata =  array(
				'id' => $post['idPort'],
				'titulo' => urls_amigables($post['titulo']),
				'url' => $post['url'],
				'categoria' => urls_amigables($post['idCat'])
			);
	    	$buscFotos[] = $newdata; //agregar
	    }
	    $smarty->assign("busqueda",$buscFotos);
	}
}
  //mysql_free_result($result);
$paginado = '<!--- pag --->
<nav class="navigation posts-navigation" role="navigation">
<!--Start Pagination-->
<nav class="navigation pagination" role="navigation">
<div class="nav-links">';

if($hayposts > 10)
	$paginado.='<a href="'.$_SERVER['PHP_SELF'].'?p='.($startrow+1).'">Siguiente <i class="ribbon-icon icon-angle-right"></i> </a>';

$prev = $startrow-10;
if ($prev >= 0)
	$paginado.='<a href="'.$_SERVER['PHP_SELF'].'?p='.$prev.'" class="next page-numbers"><i class="ribbon-icon icon-angle-left"></i> Atras</a></div>
</nav>  </nav>';
$smarty->assign("paginado",$paginado);


?>