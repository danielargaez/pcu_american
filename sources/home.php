<?php
if (!defined('nokaker')) {
  die('No se aceptan kakers.');

 }
 global $conn, $page_name;

//Config File
 $titulo = $page_name;
 $smarty->assign("catGT",$_GET['catGT']);
 $smarty->assign("busget",$_GET['s']);

 function stripBBCode($text_to_search) {
   $pattern = '|[[\/\!]*?[^\[\]]*?]|si';
   $replace = '';
   return preg_replace($pattern, $replace, $text_to_search);
 }

 function shorter($text, $chars_limit)
 {
    // Check if length is larger than the character limit
  if (strlen($text) > $chars_limit)
  {
        // If so, cut the string at the character limit
    $new_text = substr($text, 0, $chars_limit);
        // Trim off white space
    $new_text = trim($new_text);
        // Add at end of text ...
    return $new_text . " [...]";
  }
    // If not just return the text as is
  else
  {
    return $text;
  }
}

//paginacion
if (!isset($_GET['p']) or !is_numeric($_GET['p'])) {
      //we give the value of the starting row to 0 because nothing was found in URL
  $startrow = 0;
    //otherwise we take the value from the URL
} else {
  $startrow = ($_GET['p'] * 10);
}
/*
if(!empty($_GET['catGT']) || !empty($_GET['s']))
{*/
  if(!empty($_GET['catGT']) || !empty($_GET['s']))
  {  
    $titulo = "Resultado de busqueda";
    $smarty->assign("enBusqueda",true);
  }

  $sql = "SELECT * FROM posts INNER JOIN categorias ON categorias.idCat = posts.idCat";
  if(!empty($_GET['catGT']))
    $sql .= " WHERE posts.idCat = ".$_GET['catGT'];
  if(!empty($_GET['s']))
    $sql .= " WHERE titulo LIKE '%".$_GET['s']."%'";
  $sql .= " ORDER BY posts.idPost DESC LIMIT $startrow, 10";
  $result = mysqli_query($conn, $sql);
 //echo $sql;
  $hayposts = 0;
  if($result)
  {
    if(mysqli_num_rows($result) > 0)
    {
      $hayposts = mysqli_num_rows($result);
      $smarty->assign("hayPosts",true);
      $buscPosts = array();
      while($post = mysqli_fetch_assoc($result))
      {
          $fechita = strtotime($post['fecha']);
          $fechitaview = strftime("%B %d, %Y", $fechita);
          $newdata =  array(
            'id' => $post['idPost'],
            'titulo' => $post['titulo'],
            'fecha' => $fechitaview,
            'contenido' => shorter(strip_tags(trim(html_entity_decode($post['contenido']))),400),
            'titulou' => urls_amigables($post['titulo']),
            'thumbail' => $post['thumbail'],
            'visitas' => bd_nice_number($post['visitas']),
            'categoria' => $post['nombre'],
            'categoriau' => urls_amigables($post['nombre'])
          );
          $buscPosts[] = $newdata;
        }
        $smarty->assign("busqueda",$buscPosts);
      }
    //mysql_free_result($result);
         $paginado = '<!--- pag --->
         <nav class="navigation posts-navigation" role="navigation">
         <!--Start Pagination-->
         
         <nav class="navigation pagination" role="navigation">
         <h2 class="screen-reader-text">Posts navigation</h2>
         <div class="nav-links">';

         if($hayposts > 10)
          $paginado.='<a href="'.$_SERVER['PHP_SELF'].'?p='.($startrow+1).'">Siguiente <i class="ribbon-icon icon-angle-right"></i> </a>';

        $prev = $startrow-10;
        if ($prev >= 0)
          $paginado.='<a href="'.$_SERVER['PHP_SELF'].'?p='.$prev.'" class="next page-numbers"><i class="ribbon-icon icon-angle-left"></i> Atras</a></div>
        </nav>  </nav>';
        $smarty->assign("paginado",$paginado);
    }
//}

    ?>