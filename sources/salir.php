<?php
if (!defined('nokaker')) {
    die('No se aceptan kakers.');
}

session_start();

// Destruir todas las variables de sesi�n.
$_SESSION = array();

// Si se desea destruir la sesi�n completamente, borre tambi�n la cookie de sesi�n.
// Nota: �Esto destruir� la sesi�n, y no la informaci�n de la sesi�n!
if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}

echo'<script>location="/";</script><a href="/">Si no te redirecciona haz clic aqui.</a>';

// Finalmente, destruir la sesi�n.
session_destroy();
?>
