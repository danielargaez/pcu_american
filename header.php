<?php

  global $YouTubeLive, $conn, $alterno, $titulo;

  //Config
  $smarty->assign("logeado",$_SESSION['logeado']);
  $smarty->assign("action", $_GET['action']);
  $smarty->assign("actionp", $_GET['p']);
  $smarty->assign("alterno", $alterno); 
  $smarty->assign("pagename", $titulo);
  $smarty->assign("textoaviso", $texto_aviso);

//Obtenemos Fecha
  $fecha = utf8_encode(strftime("%A %d de %B del %Y"));
  $smarty->assign("fechita", $fecha);
//Obtenemos Menu P
  $smarty->assign("MenuP", menuP());

//Obtenemos news top
 $sql = "SELECT news, news3, news2, descSitio FROM configs";
 $result = mysqli_query($conn, $sql);
 $configs = mysqli_fetch_assoc($result);

 $newstop = array();
 $ns = preg_split("[\n]", $configs['news2']);
 foreach ($ns as $i => $news)
 {
    if (trim($news) != '')
    { 
      $li = $ns[$i];
      array_push($newstop, $li);
   }
 }

 $smarty->assign("anunciostop",$newstop);
 $smarty->assign("descSitio",$configs['descSitio']);

 $newsderechaex = array();
 $ns = preg_split("[\n]", $configs['news']);
   foreach ($ns as $i => $news)
   {
      if (trim($news) != '')
      {
          $video = true;
          if (strpos($ns[$i], 'youtube.com')){
              $ns[$i] = str_replace('watch?v=', '', $ns[$i]);
              $ns[$i] = str_replace('embed/', '', $ns[$i]);
              $ns[$i] = str_replace('youtube.com/', 'youtube.com/embed/', $ns[$i]);
          }
          else if(strpos($ns[$i], 'youtu.be')) {
              $ns[$i] = str_replace('watch?v=', '', $ns[$i]);
              $ns[$i] = str_replace('embed/', '', $ns[$i]);
              $ns[$i] = str_replace('youtu.be/', 'youtube.com/embed/', $ns[$i]);
          }
          else 
            $video = false;

          $newdata =  array(
            'url' => $ns[$i],
            'video' => $video
          );
          $newsderechaex[] = $newdata;
      }
   }
   $smarty->assign("anunciosderechaex",$newsderechaex);


 //PUBLICIDAD DERECHA
 if($_GET['action'] == 'admin')
 {
   $newsderecha = array();
   $ns = preg_split("[\n]", $configs['news']);
   foreach ($ns as $i => $news)
   {
      if (trim($news) != '')
      {   
                    $li = '<div style="margin-bottom: 2ex;background: white;padding: 9px;border-radius: 8px;border: 1px solid #ccc;">';
                      if (strpos($ns[$i], 'youtube.com')){
                          $ns[$i] = str_replace('watch?v=', '', $ns[$i]);
                          $ns[$i] = str_replace('embed/', '', $ns[$i]);
                          $ns[$i] = str_replace('youtube.com/', 'youtube.com/embed/', $ns[$i]);
                          $li.='<label><h6><input type="checkbox" name="remove[]" value="'.$i.'" class="check" /> Video:</h6><center><iframe width="300" height="250" src="'.$ns[$i].'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
                      }
                      else if(strpos($ns[$i], 'youtu.be')) {
                          $ns[$i] = str_replace('watch?v=', '', $ns[$i]);
                          $ns[$i] = str_replace('embed/', '', $ns[$i]);
                          $ns[$i] = str_replace('youtu.be/', 'youtube.com/embed/', $ns[$i]);
                          $li.='<label><h6><input type="checkbox" name="remove[]" value="'.$i.'" class="check" /> Video:</h6><center><iframe width="300" height="250" src="'.$ns[$i].'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
                      }
                      else{
                        $li.='<label><h6><input type="checkbox" name="remove[]" value="'.$i.'" class="check" /> Imagen:</h6><center><img src="'.$ns[$i].'" style="width:300px;height:250px;">';
                      }
                      $li.='</center><br>
                      <input name="news[]" onfocus="this.select();" style="width: 
                      100%;" type="text" value="'.$ns[$i].'"></label></div>                        
                      ';
                  array_push($newsderecha, $li);
        }              
   }
   $smarty->assign("anunciosderecha",$newsderecha);
   //anuncios
   $anunciosupderecha = array();
   $ns = preg_split("[\n]", $configs['news2']);
            foreach ($ns as $i => $news)
            {
                 if (trim($news) != '')
                 {   $li = '<div style="margin-bottom: 2ex;background: white;padding: 9px;border-radius: 8px;border: 1px solid #ccc;">

                
                <label><h6><input type="checkbox" name="remove2[]" value="'.$i.'" class="check" /> Imagen:</h6>
                <center><img src="'.$ns[$i].'" style="width:730px;height:140px;"></center><br>
                <input name="news2[]" onfocus="this.select();" style="width: 
                100%;" type="text" value="'.$ns[$i].'"></label></div>                        
                ';
                  array_push($anunciosupderecha, $li);
                }
            }
   $smarty->assign("anunciosupderecha",$anunciosupderecha);

  //anuncios portada
   $anunciosportadaad = array();
   $ns = preg_split("[\n]", $configs['news3']);
            foreach ($ns as $i => $news)
            {
                 if (trim($news) != '')
                  {  $li = '<div style="margin-bottom: 2ex;background: white;padding: 9px;border-radius: 8px;border: 1px solid #ccc;">

                
                <label><h6><input type="checkbox" name="remove3[]" value="'.$i.'" class="check" /> Imagen:</h6>
                <center><img src="'.$ns[$i].'" style="width:730px;height:140px;"></center><br>
                <input name="news3[]" onfocus="this.select();" style="width: 
                100%;" type="text" value="'.$ns[$i].'"></label></div>                        
                ';
                  array_push($anunciosportadaad, $li);
                }
            }
   $smarty->assign("anunciosportadaad",$anunciosportadaad);
 }

//Obtenemos Categorias
$sql = "SELECT idCat, nombre FROM categorias ORDER BY cOrden ASC";
$result = mysqli_query($conn, $sql);
$categorias = array();
while($result && $cat = mysqli_fetch_assoc($result)) 
{
  $newdata =  array(
    'id' => $cat['idCat'],
    'nombre' => $cat['nombre'],
    'nombreu' => urls_amigables($cat['nombre']),
  );
  $categorias[] = $newdata;
}
$smarty->assign("categorias",$categorias);

//Recientes
$sql = "SELECT posts.idPost, posts.titulo, categorias.nombre FROM posts ";
$sql .="INNER JOIN categorias ON categorias.idCat = posts.idCat";
$sql .=" ORDER BY idPost DESC LIMIT 5";
$result = mysqli_query($conn, $sql);
$recientes = array();
while($result && $post = mysqli_fetch_assoc($result)) 
{
  $newdata =  array(
    'id' => $post['idPost'],
    'titulo' => $post['titulo'],
    'titulou' => urls_amigables($post['titulo']),
    'categoria' => urls_amigables($post['nombre'])
  );
  $recientes[] = $newdata;
}
$smarty->assign("recientes",$recientes);

//Radio Check
$server = "162.248.91.166"; //IP (x.x.x.x or domain name)
$iceport = "8000"; //Port
$iceurl = "stream"; //Mountpoint
$ice_status = true;
if($fp = @fsockopen($server, $iceport, $errno, $errstr, '1')) {
fclose($fp);
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_URL, 'http://www.radiotvpeninsulayucatan.com:8000/status-json.xsl');
  $result = curl_exec($ch);
  curl_close($ch);

  $obj = json_decode($result);
  $current_song = $obj->icestats->source->title;
  $source = $obj->icestats->source;
  if(empty($source))
      $ice_status = false;

  if(empty($current_song))  
    $current_song = "Radio TV Peninsula Yucatan";

 } else {      
    $ice_status = false;
}
$smarty->assign("currentsong",$current_song);
$smarty->assign("radiostatus",$ice_status);


?>